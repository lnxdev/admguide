# AdmGuide

A collection of mini how-to.
 
---

## Details

|  EN - ENGLISH  |  PL - POLISH  |  
|----------------|---------------|

---

### Business Tools
- [Basic installation of Dolibarr ERP & CRM on Ubuntu 18.04.1 LTS](https://gitlab.com/lnxdev/admguide/blob/master/BusinessTools/dolibarr-erp-crm-ubuntu-1804.md) (EN)
- [vTiger CRM on Ubuntu 18.10](https://gitlab.com/lnxdev/admguide/blob/master/BusinessTools/vtiger-crm-ubuntu-1810.md) (EN)
- [Shopware Community Edition (CE) on Centos 7](https://gitlab.com/lnxdev/admguide/blob/master/BusinessTools/shopware-nginx-centos-7.md) (EN)
- [Metabase Business Analytics on Ubuntu 20.04](https://gitlab.com/lnxdev/admguide/blob/master/BusinessTools/metabase-business-analytics-on-ubuntu-2004.md) (EN)

---

### CMS
- [Microweber CMS on Ubuntu 18.10](https://gitlab.com/lnxdev/admguide/blob/master/CMS/microweber-ubuntu-1810.md) (EN)
- [Plone CMS on Ubuntu 18.10](https://gitlab.com/lnxdev/admguide/blob/master/CMS/plone-ubuntu-1810.md) (EN)
- [MediaWiki on CentOS 7](https://gitlab.com/lnxdev/admguide/blob/master/CMS/mediawiki-centos-7.md) (EN)
- [WordPress 5 with Nginx, MariaDB and PHP7 on Debian 9 and Ubuntu 18.04/18.10](https://gitlab.com/lnxdev/admguide/blob/master/CMS/wordpress-nginx-mariadb-php7-debian9-ubuntu18x.md) (EN)
- [OctoberCMS on Ubuntu 18.04](https://gitlab.com/lnxdev/admguide/blob/master/CMS/octobercms-ubuntu-1804.md) (EN)
- [Anchor CMS on Debian 9](https://gitlab.com/lnxdev/admguide/blob/master/CMS/anchorcms-debian-9.md) (EN)
- [WonderCMS on Ubuntu 19.04 and Debian 9](https://gitlab.com/lnxdev/admguide/blob/master/CMS/wondercms-nginx-ubuntu-1904.md) (EN)
- [DokuWiki on Ubuntu 18.04](https://gitlab.com/lnxdev/admguide/blob/master/CMS/dokuwiki-ubuntu-1804.md) (EN)
- [WonderCMS on Fedora 29/30](https://gitlab.com/lnxdev/admguide/blob/master/CMS/wondercms-nginx-fedora-29.md) (EN)
- [Automad on Ubuntu 19.04](https://gitlab.com/lnxdev/admguide/blob/master/CMS/automadcms-nginx-ubuntu-1904.md) (EN)
- [Grav CMS on Ubuntu 19.04](https://gitlab.com/lnxdev/admguide/blob/master/CMS/gravcms-nginx-ubuntu-1904.md) (EN)
- [Pico CMS on Debian 10](https://gitlab.com/lnxdev/admguide/blob/master/CMS/picocms-nginx-debian-10.md) (EN)
- [Drupal on Debian 10 - quick install](https://gitlab.com/lnxdev/admguide/blob/master/CMS/drupal-quick-debian-10.md) (EN)

---

### HA / Monitoring / Security / System Management
- [How to setup DRBD to replicate storage on CentOS 7 Servers](https://gitlab.com/lnxdev/admguide/blob/master/HA/drbd-two-centos-7.md) (EN)
- [Zabbix on Debian 10](https://gitlab.com/lnxdev/admguide/blob/master/Monitoring/zabbix-debian-10.md) (EN)
- [Kerberos Server and Client on Ubuntu 18.04 LTS](https://gitlab.com/lnxdev/admguide/blob/master/Security/kerberos-server-client-ubuntu-1804.md) (EN)
- [Ansible AWX on Ubuntu 18.04 LTS](https://gitlab.com/lnxdev/admguide/blob/master/SystemManagement/ansible-awx-ubuntu-1804.md) (EN)

---

### Web Servers
- [Nginx with virtual hosts and self-signed ssl certificate](https://gitlab.com/lnxdev/admguide/blob/master/WebServers/nginx-virtualhosts-selfssl-centos-7.md) (EN)
- [Short instruction how to install ssl certificate from Let’s Encrypt on Apache Tomcat](https://gitlab.com/lnxdev/admguide/blob/master/WebServers/sslcertificate-letsencrypt-tomcat-debian-9.md) (EN)
- [Apache Tomcat 9 on Ubuntu 18.10](https://gitlab.com/lnxdev/admguide/blob/master/WebServers/tomcat9-ubuntu-1810.md) (EN)
- [Build the latest mainline version Nginx from source on Fedora 29](https://gitlab.com/lnxdev/admguide/blob/master/WebServers/build-nginx-fedora-29.md) (EN)
- [Nginx with PHP and MySQL on Ubuntu 18.04 LTS](https://gitlab.com/lnxdev/admguide/blob/master/WebServers/nginx-php-mysql-ubuntu-1804.md) (EN)
- [OpenLiteSpeed Web Server on Debian 10](https://gitlab.com/lnxdev/admguide/blob/master/WebServers/openlitespeed-server-debian-10.md) (EN)
- [Lighttpd with PHP and MariaDB on Debian 10](https://gitlab.com/lnxdev/admguide/blob/master/WebServers/lighttpd-php-mariadb-debian-10.md) (EN)