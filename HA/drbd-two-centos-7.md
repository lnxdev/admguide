### How to setup DRBD to replicate storage on CentOS 7 Servers

> My test platform: CentOS Linux release 7.6.1810 (Core)

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.
- Cluster nodes with the same sized storage area for DRBD. This can be a hard drive partition (or a full physical hard drive), a software RAID device, an LVM Logical Volume or a any other block device type found on your system. 

In this how-to, I am using two nodes:

| node  |      ip       |       hostname       | hard drive partition for DRBD |
|-------|---------------|----------------------|-------------------------------|
| node1 | 192.168.1.215 | ctx15vm.local.lnxorg | /dev/xvdb1                    |
| node2 | 192.168.1.216 | ctx16vm.local.lnxorg | /dev/xvdb1                    |

#### 2. Installing DRBD packages (on both nodes)

DRBD can be installed from the ELRepo or EPEL repositories. In this example, I used ELRepo repositories:

```
# rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
# rpm -ivh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
# yum update -y
```

Installation of the kernel module and tools for DRBD:

```
# yum install -y kmod-drbd84 drbd84-utils
```

If you have SELinux enabled, you need to modify the policies:

```
# semanage permissive -a drbd_t
```

If your system has a firewall enabled (firewalld), you need to add the DRBD port 7789 in the firewall to allow synchronization of data between the two nodes.

My example for node1:

```
# firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address="192.168.1.216" port port="7789" protocol="tcp" accept'
# firewall-cmd --reload
```

My example for node2:

```
# firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address="192.168.1.215" port port="7789" protocol="tcp" accept'
# firewall-cmd --reload
```

#### 3: Configuring DRBD (on both nodes)

DRBD’s main configuration is located at */etc/drbd.conf* and additional config files can be found in the */etc/drbd.d/* directory.

To replicate storage, we need to add the necessary configurations in the */etc/drbd.d/global_common.conf* file which contains the global and common sections of the DRBD configuration and we need define resources in *.res* files.

Make a backup of the original file:

```
# mv /etc/drbd.d/global_common.conf /etc/drbd.d/global_common.conf.orig
```

Open a new file */etc/drbd.d/global_common.conf* in text editor and add the following lines:

```
global {
 usage-count yes;
}
common {
 net {
  protocol C;
 }
}
```

**Explanation for the *protocol*.**

DRBD supports three replication modes:
- **protocol A** - asynchronous replication protocol. It’s most often used in long distance replication scenarios.
- **protocol B** - semi-synchronous replication protocol aka *memory synchronous protocol*.
- **protocol C** - commonly used for nodes in short distanced networks. The most commonly used replication protocol in DRBD setup.

The choice of replication protocol influences two factors of your deployment: protection and latency.

#### 3.1. Adding a resource (on both nodes)

Add the following content to the */etc/drbd.d/test.res* file. Remember to replace the variables with the actual values for your environment (below example for my nodes).

```
resource test {
  on ctx15vm.local.lnxorg {
    device /dev/drbd0;
    disk /dev/xvdb1;
    meta-disk internal;  
    address 192.168.1.215:7789;
  }
  on ctx16vm.local.lnxorg {
    device /dev/drbd0;
    disk /dev/xvdb1;
    meta-disk internal;
    address 192.168.1.216:7789;
  }
}
```

where:

- on *hostname* - node hostname
- resource *test* - the name of the resource
- *device /dev/drbd0* - the virtual block device managed by DRBD
- *disk /dev/xvdb1* - the block device partition which is the backing device for the DRBD device
- *meta-disk* - defines where DRBD stores its metadata (*Internal* means that DRBD stores its meta data on the same physical device)
- *address* - IP address and port number of the respective node

#### 3.2 Initializing and enabling resource (on both nodes)

After adding all the initial resource configurations, you must bring up the resource:

```
# drbdadm create-md test
```

In response, you'll see something like that (example answer for ctx15vm.local.lnxorg):

```
[root@ctx15vm serwis]# drbdadm create-md test
initializing activity log
initializing bitmap (32 KB) to all zero
Writing meta data...
New drbd meta data block successfully created.
success
[root@ctx15vm serwis]#
```

Next, you should enable the resource:

```
# drbdadm up test
```

Run the *lsblk* command. You will notice that the DRBD device/volume **drbd0** is associated with the backing device */dev/xvdb1* (in my example). Example answer for ctx15vm.local.lnxorg:

```
[root@ctx15vm serwis]# lsblk
NAME      MAJ:MIN RM    SIZE RO TYPE MOUNTPOINT
sr0        11:0    1   1024M  0 rom
xvda      202:0    0     15G  0 disk
├─xvda1   202:1    0   14,7G  0 part /
└─xvda2   202:2    0    359M  0 part [SWAP]
xvdb      202:16   0      1G  0 disk
└─xvdb1   202:17   0   1024M  0 part
  └─drbd0 147:0    0 1023,9M  1 disk
[root@ctx15vm serwis]# 
```

To check the resource status, run the following command (the state of the *Inconsistent* disk will be normal at this point):

```
# drbdadm status test
```

or:

```
# drbdsetup status test --verbose --statistics
```

In response, you'll see something like that (example answer for ctx15vm.local.lnxorg):

```
[root@ctx15vm serwis]# drbdadm status test
test role:Secondary
  disk:Inconsistent
  peer role:Secondary
    replication:Established peer-disk:Inconsistent

[root@ctx15vm serwis]# drbdsetup status test --verbose --statistics
test role:Secondary suspended:no
    write-ordering:flush
  volume:0 minor:0 disk:Inconsistent
      size:1048476 read:0 written:0 al-writes:8 bm-writes:0 upper-pending:0 lower-pending:0 al-suspended:no
      blocked:no
  peer connection:Connected role:Secondary congested:no
    volume:0 replication:Established peer-disk:Inconsistent resync-suspended:no
        received:0 sent:0 out-of-sync:1048476 pending:0 unacked:0

[root@ctx15vm serwis]#
```

#### 4. Set (primary resource) source of initial device synchronization (on only one node)

Define which node should be used as the source of the initial device synchronization. Run the following command to start the initial full synchronization:

```
# drbdadm primary --force test
# drbdadm status test
```

In response, you'll see something like that (example answer for ctx15vm.local.lnxorg):

```
[root@ctx15vm serwis]# drbdadm status test
test role:Primary
  disk:UpToDate
  peer role:Secondary
    replication:SyncSource peer-disk:Inconsistent done:4.87

[root@ctx15vm serwis]#
```

After synchronization, the status of disks on nodes should have the value *UpToDate*.

#### 5. Starting and enabling the DRBD daemon (on both nodes)

```
# systemctl start drbd.service
# systemctl status drbd.service
# systemctl enable drbd.service
```

#### 6. Testing DRBD Setup

Now we can create a filesystem on the device */dev/drbd0*, on the node where we started the initial full synchronization (resource with **primary role**) and mount it as shown:

```
# mkfs -t ext4 /dev/drbd0 
# mkdir -p /mnt/DRDB_PRI/
# mount /dev/drbd0 /mnt/DRDB_PRI/
```

Now copy or create files at the above mount point and execute *ls* to make sure everything went OK. Next, unmount the device and change the role of the node from primary to secondary:

```
# umount /mnt/DRDB_PRI/
# drbdadm secondary test
```

On the other node (which has the resource with a secondary role), make it primary, then mount the device on it and execute *ls* of the mount point. If the setup is working fine, all the files stored in the volume should be there:

```
# drbdadm primary test
# mkdir -p /mnt/DRDB_SEC/
# mount /dev/drbd0 /mnt/DRDB_SEC/
# ls -l /mnt/DRDB_SEC/
```

Reference: [The DRBD User’s Guide](https://docs.linbit.com/docs/users-guide-8.4/).

---

### Update 1

**a)** If the options have equal values on both hosts, you can specify them directly in the resource section. For example, the configuration from point **3.1** can be restructured on:

```
resource test {
  device /dev/drbd0;
  disk /dev/sdb1;
    meta-disk internal;  
    on ctx15vm.local.lnxorg {
      address 192.168.1.215:7789;
    }
    on ctx16vm.local.lnxorg {
      address 192.168.1.216:7789;
    }
}
```

**b)** To turn off the resource, run:

```
# drbdadm down test
```
