### Pico CMS on Debian 10

> My test platform: Debian 10

Pico is an open-source simple and fast flat-file CMS written in PHP. This means there is no administration backend and database to deal with. You simply create *.md* files in the content folder, and that becomes a page. 

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### Requirements for installing and running Pico CMS

- PHP version 5.3.6 or greater.
- Web server: Apache or Nginx.
- Composer.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install -y wget vim git unzip mc curl net-tools socat bash-completion apt-transport-https
```

#### 2. Nginx with PHP - installation and configuration

```
$ sudo apt install -y nginx php7.3 php7.3-cli php7.3-fpm php7.3-common php7.3-curl php7.3-zip php7.3-mbstring \
                        php7.3-gd php7.3-xml php7.3-json
$ sudo systemctl start nginx.service                        
$ systemctl status nginx.service
$ sudo systemctl enable nginx.service                        
$ sudo systemctl start php7.3-fpm.service
$ systemctl status php7.3-fpm.service
$ sudo systemctl enable php7.3-fpm.service
```

Create the virtual host file */etc/nginx/sites-available/pico.conf*. For the purposes of this tutorial, I will use the *ctx00vm.local.lnxorg* domain, you can change it to the domain you want to use.

```
server {

  listen 80;

  server_name ctx00vm.local.lnxorg;
  root /var/www/pico;

  index index.php;

  location ~ ^/((config|content|vendor|composer\.(json|lock|phar))(/|$)|(.+/)?\.(?!well-known(/|$))) {
    deny all;
  }

  location / {
    index index.php;
    try_files $uri $uri/ /index.php$is_args$args;
  }

  location ~ \.php$ {
    try_files $uri =404;
    fastcgi_index index.php;
    fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    include fastcgi_params;
    fastcgi_param PICO_URL_REWRITING 1;
  }

}
```

Enable the site and restart the Nginx configuration:

```
$ sudo ln -s /etc/nginx/sites-available/pico.conf /etc/nginx/sites-enabled/
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ systemctl status nginx.service
```

#### 3. Composer installation

```
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
$ sudo mv composer.phar /usr/local/bin/composer
$ composer --version
```

**NOTE**: Current installation commands for Composer can be found at [https://getcomposer.org/download/](https://getcomposer.org/download/).

#### 4. Pico installation

```
$ sudo mkdir -p /var/www/pico
$ sudo chown -R $USER: /var/www/pico
$ cd /var/www/pico
$ composer create-project picocms/pico-composer . 
$ sudo chown -R www-data: /var/www/pico
```

Create your own folder in the Pico directory and add .md files with the content to display. They will automatically become your pages.

Reference: 
- [Pico CMS](http://picocms.org/)
- [Github Pico CMS](https://github.com/picocms/Pico)
