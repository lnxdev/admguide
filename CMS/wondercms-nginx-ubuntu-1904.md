
### WonderCMS on Ubuntu 19.04 and Debian 9

> My test platform: Ubuntu Server 19.04, Debian 9.9 and WonderCMS 2.7.0

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### Requirements for installing and running WonderCMS

- PHP version 7.1 or greater with the curl, mbstring and zip extensions.
- Web server (Apache with mod_rewrite module enabled, Nginx, IIS).

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install -y wget vim git unzip mc curl net-tools socat bash-completion apt-transport-https
```

#### NOTE - For Debian 9:

```
$ sudo apt install lsb-release
$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
$ echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
$ sudo apt update
```

#### 2. Nginx with PHP - installation and configuration

```
$ sudo apt install -y nginx php7.2 php7.2-cli php7.2-fpm php7.2-common php7.2-curl php7.2-zip php7.2-mbstring
$ systemctl status nginx.service
$ systemctl status php7.2-fpm.service
```

On Ubuntu 19.04 and Debian 9, services should be started and turned on automatically after packages pre-configuration is complete, so there is no need to start and enable them manually.

Create the virtual host file */etc/nginx/sites-available/wondercms.conf*. For the purposes of this tutorial, I will use the *ctx11vm.local.lnxorg* domain, you can change it to the domain you want to use.

```
server {

  listen 80;

  server_name ctx11vm.local.lnxorg;
  root /var/www/wondercms;

  index index.php;

  location / {
    if (!-e $request_filename) {
      rewrite ^/(.+)$ /index.php?page=$1 last;
    }
  }
  location ~ database.js {
    return 403;
  }

  location ~ \.php(/|$) {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
  }

}
```

Enable the site and reload the Nginx configuration:

```
$ sudo ln -s /etc/nginx/sites-available/wondercms.conf /etc/nginx/sites-enabled/
$ sudo nginx -t
$ sudo systemctl reload nginx.service
$ systemctl status nginx.service
```

#### 3. WonderCMS - installation:

```
$ sudo mkdir -p /var/www/wondercms
$ cd /var/www/wondercms
$ sudo wget https://github.com/robiso/wondercms/releases/download/2.7.0/WonderCMS-2.7.0.zip
$ sudo unzip WonderCMS-2.7.0.zip
$ sudo rm WonderCMS-2.7.0.zip
$ cd wondercms
$ sudo cp -r . ../
$ cd ../
$ sudo rm -rf wondercms
$ sudo chown www-data: /var/www/wondercms/ -R
```

Open the URL of your WonderCMS in your web browser and log in with the default password *admin*. Then change them.

Reference: 
- [WonderCMS](https://www.wondercms.com/)
- [Github](https://github.com/robiso/wondercms)
