### OctoberCMS on Ubuntu 18.04

> My test platform: Ubuntu Server 18.04.2

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### 2. Apache, MariaDB, PHP - installation and configuration

```
$ sudo apt update && sudo apt upgrade
$ sudo apt install apache2 mariadb-server php7.2 libapache2-mod-php7.2 libapache2-mod-php7.2 php7.2-json php7.2-common \
                    php7.2-mbstring php7.2-xmlrpc php7.2-soap php7.2-gd php7.2-xml php7.2-intl php7.2-tidy php7.2-mysql \
                    php7.2-cli php7.2-ldap php7.2-pdo php7.2-zip php7.2-curl php7.2-sqlite3 unzip wget -y
```

Once the installation has been completed, edit */etc/php/7.2/apache2/php.ini* and set:

```
...
file_uploads = On
...
allow_url_fopen = On
...
memory_limit = 256M
...
upload_max_filesize = 30M
...
post_max_size = 40M
...
max_execution_time = 60
...
max_input_vars = 1500
...
date.timezone = Europe/Warsaw
...
```

**date.timezone* - yours timezone

Restart the Apache service. On Ubuntu, the services should be started automatically after packages pre-configuration is complete. You can check it and enable the start of services together with the system.

```
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
$ sudo systemctl enable apache2.service
$ sudo systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

#### 3. MariaDB configuration

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 35
Server version: 10.1.38-MariaDB-0ubuntu0.18.04.1 Ubuntu 18.04

Copyright (c) 2000, 2017, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [mysql]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> exit
Bye
```

Restart MariaDB service:

```
$ sudo systemctl restart mariadb.service
$ sudo systemctl status mariadb.service
```

Log in to MariaDB shell, create a database and user for OctoberCMS. Grant privileges to the database. In this example:

- **octoberdb** - database for OctoberCMS
- **octoberusr** - user for OctoberCMS database
- **pass4octoberdb** - secure password for octoberusr and octoberdb

```
$ mysql -u root -p
Enter password:
MariaDB [(none)]> CREATE DATABASE octoberdb;
MariaDB [(none)]> CREATE USER 'octoberusr'@'localhost' IDENTIFIED BY 'pass4octoberdb';
MariaDB [(none)]> GRANT ALL PRIVILEGES ON octoberdb.* TO 'octoberusr'@'localhost' IDENTIFIED BY 'pass4octoberdb' WITH GRANT OPTION;
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```

#### 4. OctoberCMS - download and installation

```
$ cd /tmp
$ wget https://codeload.github.com/octobercms/install/zip/master -O octobercms.zip
$ unzip octobercms.zip
$ sudo mv install-master /var/www/html/octobercms
$ sudo chown -R www-data:www-data /var/www/html/octobercms
$ sudo chmod -R 755 /var/www/html/octobercms
```

Next, create an Apache virtual host file */etc/apache2/sites-available/octobercms.conf*:

```
VirtualHost *:80>
     ServerAdmin admin@local.lnxorg
     ServerName ctx01vm.local.lnxorg
     DocumentRoot /var/www/html/octobercms/

     <Directory /var/www/html/octobercms/>
        Options +FollowSymlinks
        AllowOverride All
        Require all granted
     </Directory>

     ErrorLog /var/log/apache2/october_error.log
     CustomLog /var/log/apache2/october_access.log combined
</VirtualHost>
```

Replace *ServerAdmin* and *ServerName* with parameters corresponding to your case. Save the file and enable the virtual host, the Apache rewrite module and restart web server to apply all the changes:

```
$ sudo a2ensite octobercms
$ sudo a2dissite 000-default
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
```

#### 5. OctoberCMS configuration

- Open your web browser and type the URL of your OctoberCMS website.
- Make sure all requirements are met and accept the licence agreement.
- Provide your database details like: 
  - database type ( *MySQL* ), 
  - database host ( *localhost* ) 
  - MySQL Port ( *3306* )
  - database name ( *octoberdb* ), 
  - database username ( *octoberusr* ),
  - MySQL password ( *pass4octoberdb* ). 
- Click on the **Administrator** button. Specify details for logging in to *Administration Area*:
  - admin username,
  - admin email address,
  - admin password. 
- Click on the **Continue** button.
- Click on the **Start from a theme** button.
- Install the plugins as per your need. 
- After installation, go to the **Administration area**.
- Provide your *admin username* and *password*, then click on the **Login** button. You will be redirected to the OctoberCMS dashboard.

Reference: [OctoberCMS DOCS](https://octobercms.com/docs/setup/installation).
