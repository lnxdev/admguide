### WordPress 5 with Nginx, MariaDB and PHP7 on Debian 9 and Ubuntu 18.04/18.10

> My test platform: Debian 9.8, Ubuntu Server 18.04.2/18.10

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### 2. Nginx and PHP7 - installation and configuration

```
$ sudo apt update && sudo apt upgrade
$ sudo apt install -y nginx php-fpm php-common php-mbstring php-xmlrpc php-soap php-gd php-xml php-intl php-mysql php-cli php-ldap php-zip php-curl
```

The services should be started automatically after packages pre-configuration is complete. You can check it and enable the start of services together with the system.

```
$ systemctl status nginx.service
$ sudo systemctl enable nginx.service
```

#### NOTE - For Debian9:

```
$ systemctl status php7.0-fpm.service
$ sudo systemctl enable php7.0-fpm.service
```

#### NOTE - For Ubuntu 18.04/18.10:

```
$ systemctl status php7.2-fpm.service
$ sudo systemctl enable php7.2-fpm.service
```

Create a Nginx virtual host file */etc/nginx/sites-available/ctx04vm.local.lnxorg.conf*. For the purpose of this tutorial, I will use *ctx04vm.local.lnxorg* domain, you can change it to the domain you want to use.

#### NOTE - For Debian 9:

```
server {
    listen 80;
    listen [::]:80;
    root /var/www/html/wordpress;
    index  index.php index.html index.htm;
    server_name ctx04vm.local.lnxorg;

    client_max_body_size 100M;

    location / {
        try_files $uri $uri/ /index.php?$args;        
    }

    location ~ \.php$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass             unix:/var/run/php/php7.0-fpm.sock;
    fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

#### NOTE - For Ubuntu 18.04/18.10 change fastcgi_pass:

```
...
fastcgi_pass             unix:/var/run/php/php7.2-fpm.sock;
...
```

Enable the site and reload Nginx:

```
$ sudo ln -s /etc/nginx/sites-available/ctx04vm.local.lnxorg.conf  /etc/nginx/sites-enabled/
$ sudo systemctl reload nginx.service
$ systemctl status nginx.service
```

#### 3. MariaDB - installation and configuration

```
$ sudo apt install -y mariadb-server
```

When the installation has finished, enable the service so it will be available after each system boot.

```
$ systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

To secure MariaDB installation, use the command below:

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 10
Server version: 10.1.37-MariaDB-0+deb9u1 Debian 9.6

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [mysql]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> exit
Bye
```

Restart MariaDB service:

```
$ sudo systemctl restart mariadb.service
$ systemctl status mariadb.service
```

Log in to MariaDB shell, create a database and user for WordPress. Grant privileges to the database. In this example:

- **wordpressdb** - database for WordPress
- **wordpressusr** - user for WordPress database
- **pass4wordpressdb** - secure password for wordpressusr and wordpressdb

```
$ mysql -u root -p
Enter password:
MariaDB [(none)]> CREATE DATABASE wordpressdb;
MariaDB [(none)]> CREATE USER 'wordpressusr'@'localhost' IDENTIFIED BY 'pass4wordpressdb';
MariaDB [(none)]> GRANT ALL ON wordpressdb.* TO 'wordpressusr'@'localhost';
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```

#### 4. WordPress - installation:

```
$ cd /tmp && wget http://wordpress.org/latest.tar.gz
$ sudo tar -xvzf latest.tar.gz -C /var/www/html
$ sudo chown www-data: /var/www/html/wordpress/ -R
```

#### 4.1 WordPress configuration:

- Open your web browser and type the URL of your WordPress website.
- Choose the language settings. Click on the **Continue** button.
- Click on the **Let's go!** button.
- Provide your database details like *database name*, *database username*, and *password*. Click on the **Submit** button.
- Click on the **Run the install** button.
- Give your site a name, create administrative username and password, enter your email address. Click on the **Install WordPress** button.
- WordPress will be installed and you will see a success screen. Now you can log in to your site by clicking the **Log In** button and entering the credentials you provided during WordPress installation.
