### Automad on Ubuntu 19.04

> My test platform: Ubuntu Server 19.04

Automad is a flat open content management system and template mechanism written in PHP in which all content is stored in human-readable text files.

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### Requirements for installing and running Automad

- PHP version 5.4 or greater.
- Web server: Apache or Nginx.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install -y wget vim git unzip mc curl net-tools socat bash-completion apt-transport-https
```

#### 2. Nginx with PHP - installation and configuration

```
$ sudo apt install -y nginx php7.2 php7.2-cli php7.2-fpm php7.2-common php7.2-curl php7.2-zip php7.2-mbstring \
                            php7.2-xmlrpc php7.2-soap php7.2-gd php7.2-xml
$ systemctl status nginx.service
$ systemctl status php7.2-fpm.service
```

On Ubuntu 19.04 services should be started and turned on automatically after packages pre-configuration is complete, so there is no need to start and enable them manually.

Create the virtual host file */etc/nginx/sites-available/automad.conf*. For the purposes of this tutorial, I will use the *ctx15vm.local.lnxorg* domain, you can change it to the domain you want to use.

```
server {

  listen 80;

  server_name ctx15vm.local.lnxorg;
  root /var/www/automad;

  index index.php index.html;

  client_max_body_size 100M;

  location / {
    try_files $uri $uri/ /index.php$is_args$args;
  }

  location ~ \.php$ {
    fastcgi_index index.php;
    fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    include fastcgi_params;
  }

}
```

Enable the site and reload the Nginx configuration:

```
$ sudo ln -s /etc/nginx/sites-available/automad.conf /etc/nginx/sites-enabled/
$ sudo nginx -t
$ sudo systemctl reload nginx.service
$ systemctl status nginx.service
```

#### 3. Automad - installation and configuration:

```
$ sudo mkdir -p /var/www/automad
$ cd /var/www/automad
$ sudo curl -O -J -L https://automad.org/download
$ sudo unzip marcantondahmen-automad-6fff2a0456dc.zip
$ sudo rm -f marcantondahmen-automad-6fff2a0456dc.zip
$ cd marcantondahmen-automad-6fff2a0456dc/
$ sudo cp -r . ../
$ cd ../
$ sudo rm -rf marcantondahmen-automad-6fff2a0456dc/ 
$ sudo chown www-data: /var/www/automad/ -R
```

Open the URL of your Automated in your web browser. Go to *http://your-url/dashboard* and follow the instructions. Create a user account using the form and download the generated file to your computer. After that you have to move the downloaded file to the */config* directory within Automad installation directory. After that you can log in to **Automad Dashboard**.

Reference: 
- [Automated CMS](https://automad.org/)
- [Automated Bitbucket](https://bitbucket.org/marcantondahmen/automad/)
