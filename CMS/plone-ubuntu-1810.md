### Plone CMS on Ubuntu 18.10

> My test platform: Ubuntu Server 18.10

#### 1. Requirements

- A non-root user with sudo privileges.

#### 2. Install required dependencies

```
$ sudo apt update -y
$ sudo apt upgrade -y
$ sudo apt install build-essential gcc libjpeg-dev wget readline-common libssl-dev libxml2-dev libxslt1-dev python2.7 python-dev python-btrees -y
```

#### 3. Plone installation

Download the Plone from *https://launchpad.net/plone* to the */tmp* directory and start installation. 

```
$ cd /tmp
$ wget https://launchpad.net/plone/5.1/5.1.5/+download/Plone-5.1.5-UnifiedInstaller.tgz
$ tar -xvzf Plone-5.1.5-UnifiedInstaller.tgz
$ sudo cp -r Plone-5.1.5-UnifiedInstaller /opt/plone
$ cd /opt/plone
$ sudo ./install.sh standalone --target=/opt/plone --password=password4plone --with-python=/usr/bin/python2.7
```

**password* - password for admin user

You should see the following output:

```
Testing /usr/bin/python2.7 for Zope/Plone requirements....
/usr/bin/python2.7 looks OK. We will use it.


Root install method chosen. Will install for use by users:
  ZEO & Client Daemons:      plone_daemon
  Code Resources & buildout: plone_buildout


Detailed installation log being written to /opt/plone/install.log
Installing Plone 5.1.5 at /opt/plone

Using useradd and groupadd to create users and groups.
Creating Python virtual environment.
New python executable in /opt/plone/zinstance/bin/python2.7
Also creating executable in /opt/plone/zinstance/bin/python
Installing setuptools, pip, wheel...
done.
Installing zc.buildout in virtual environment.
Unpacking buildout cache to /opt/plone/buildout-cache
Copying buildout skeleton
Building Zope/Plone; this takes a while...
Buildout completed
```

Once the installation has been completed successfully, you should see the following output:

```
#####################################################################

######################  Installation Complete  ######################

Plone successfully installed at /opt/plone
See /opt/plone/zinstance/README.html
for startup instructions.

Use the account information below to log into the Zope Management Interface
The account has full 'Manager' privileges.

  Username: admin
  Password: password4plone

This account is created when the object database is initialized. If you change
the password later (which you should!), you'll need to use the new password.

Use this account only to create Plone sites and initial users. Do not use it
for routine login or maintenance.- If you need help, ask in IRC channel #plone on irc.freenode.net. - The live support channel also exists at http://plone.org/chat - You can also ask for help on https://community.plone.org - Submit feedback and report errors at https://github.com/plone/Products.CMFPlone/issues (For install problems, https://github.com/plone/Installers-UnifiedInstaller/issues)
```

Start the Plone service:

```
$ sudo /opt/plone/zinstance/bin/plonectl start
```

You should see:

```
instance: . . 
daemon process started, pid=16591
```

Check the status of Plone:

```
$ sudo /opt/plone/zinstance/bin/plonectl status
```

You should see:

```
daemon process started, pid=16591
```

#### 4. Plone configuration

- Open your web browser and enter the URL of your Plone website, followed by the default service port 8080 ( *http://url-address:8080* ). 
- Click on the **Create a new Plone site** button.
- Provide your *username* and *password* (in this example username: *admin*, password: *password4plone*), then click on the **Sign In** button. 
- Provide *site id*, *site title*, *language*, *default timezone* and click on the **Create Plone Site** button. You should see the **Plone dashboard**.

Reference: [Plone Documentation](https://docs.plone.org/).
