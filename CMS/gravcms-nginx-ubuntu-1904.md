### Grav CMS on Ubuntu 19.04

> My test platform: Ubuntu Server 19.04

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### Requirements for installing and running Grav CMS

- PHP version 7.1.3 or greater.
- Web server: Apache, Nginx, LiteSpeed etc.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install -y wget vim git unzip mc curl net-tools socat bash-completion apt-transport-https
```

#### 2. Nginx with PHP - installation and configuration

```
$ sudo apt install -y nginx php7.2 php7.2-cli php7.2-fpm php7.2-common php7.2-curl php7.2-zip php7.2-mbstring \
                        php7.2-xmlrpc php7.2-soap php7.2-gd php7.2-xml php7.2-json php7.2-opcache php7.2-apcu
$ systemctl status nginx.service
$ systemctl status php7.2-fpm.service
```

On Ubuntu 19.04 services should be started and turned on automatically after packages pre-configuration is complete, so there is no need to start and enable them manually.

Create the virtual host file */etc/nginx/sites-available/gravcms.conf*. For the purposes of this tutorial, I will use the *ctx02vm.local.lnxorg* domain, you can change it to the domain you want to use.

```
server {

  listen 80;

  server_name ctx02vm.local.lnxorg;
  root /var/www/gravcms;

  index index.php;

  location / { try_files $uri $uri/ /index.php?$query_string; }
  location ~* /(\.git|cache|bin|logs|backup|tests)/.*$ { return 403; }
  location ~* /(system|vendor)/.*\.(txt|xml|md|html|yaml|yml|php|pl|py|cgi|twig|sh|bat)$ { return 403; }
  location ~* /user/.*\.(txt|md|yaml|yml|php|pl|py|cgi|twig|sh|bat)$ { return 403; }
  location ~ /(LICENSE\.txt|composer\.lock|composer\.json|nginx\.conf|web\.config|htaccess\.txt|\.htaccess) { return 403; }

  location ~ \.php$ {
    fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_index index.php;
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root/$fastcgi_script_name;
  }

}
```

Enable the site and restart the Nginx configuration:

```
$ sudo ln -s /etc/nginx/sites-available/gravcms.conf /etc/nginx/sites-enabled/
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ systemctl status nginx.service
```

#### 3. Grav CMS - installation and configuration:

```
$ sudo mkdir -p /var/www/gravcms
$ sudo chown -R $USER: /var/www/gravcms
$ cd /var/www/gravcms
$ wget https://getgrav.org/download/core/grav-admin/1.6.15
$ unzip 1.6.15
$ cd grav-admin
$ cp -r . ../
$ cd ../
$ rm -rf grav-admin/ 1.6.15
$ sudo chown -R www-data: /var/www/gravcms
```

Open the URL of your Grav CMS in your web browser and follow the instructions. Create a user account using the form and log in to **Grav Admin**.

Reference: 
- [Grav CMS](https://getgrav.org/)
- [Grav CMS Gitlab](https://github.com/getgrav/grav)
