### Microweber CMS on Ubuntu 18.10

> My test platform: Ubuntu Server 18.10

#### 1. Requirements

- A non-root user with sudo privileges.

#### 2. Installation Apache, PHP and MariaDB

```
$ sudo apt install apache2 mariadb-server php7.2 libapache2-mod-php7.2 php7.2-common php7.2-mbstring php7.2-xmlrpc \
                    php7.2-soap php7.2-mysql php7.2-gd php7.2-xml php7.2-cli php7.2-zip unzip wget -y
```

Once the installation has been completed, edit */etc/php/7.2/apache2/php.ini* and set:

```
...
memory_limit = 256M
...
upload_max_filesize = 150M
...
max_execution_time = 360
...
date.timezone = Europe/Warsaw
...
```

**date.timezone* - yours timezone

Restart Apache service. On Ubuntu, the services should be started automatically after packages pre-configuration is complete. You can check it and enable services together with the system.

```
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
$ sudo systemctl enable apache2.service
$ sudo systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

#### 3. MariaDB configuration

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 31
Server version: 10.1.29-MariaDB-6ubuntu2 Ubuntu 18.10

Copyright (c) 2000, 2017, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [mysql]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> exit
Bye
```

Restart MariaDB service:

```
$ sudo systemctl restart mariadb.service
$ sudo systemctl status mariadb.service
```

Log in to MariaDB shell, create a database and user for Microweber. Grant privileges to the database. In this example:

- **microweberdb** - database for Microweber
- **microweberusr** - user for Microweber database
- **pass4microweberdb** - secure password for microweberusr and microweberdb

```
$ mysql -u root -p
Enter password:
MariaDB [(none)]> CREATE DATABASE microweberdb;
MariaDB [(none)]> CREATE USER 'microweberusr'@'localhost' IDENTIFIED BY 'pass4microweberdb';
MariaDB [(none)]> GRANT ALL ON microweberdb.* TO 'microweberusr'@'localhost' IDENTIFIED BY 'pass4microweberdb' WITH GRANT OPTION;
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```

#### 4. Download and installation Microweber

```
$ cd /tmp
$ wget https://microweber.com/download.php -O microweber-latest.zip
$ sudo mkdir /var/www/html/microweber
$ sudo unzip microweber-latest.zip -d /var/www/html/microweber
$ sudo chown -R www-data:www-data /var/www/html/microweber/
$ sudo chmod -R 755 /var/www/html/microweber/
```

Next, create an Apache virtual host file */etc/apache2/sites-available/microweber.conf*:

```
<VirtualHost *:80>
     ServerAdmin admin@local.lnxorg
     DocumentRoot /var/www/html/microweber
     ServerName local.lnxorg
     ServerAlias www.local.lnxorg

    <Directory /var/www/html/microweber/>
        Options FollowSymlinks
        AllowOverride All
        Require all granted
     </Directory>

     ErrorLog ${APACHE_LOG_DIR}/error.log
     CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Replace *ServerAdmin*, *ServerName* and *ServerAlias* with parameters corresponding to your case. Save the file and enable the virtual host, the Apache rewrite module and restart web server to apply all the changes:

```
$ sudo a2ensite microweber.conf
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
```

#### 5. Microweber configuration

- Open your web browser and type the URL of your Microweber website (in this example *http://local.lnxorg*).
- Provide your database details like database name, database username, and password, admin username, and admin password. Click on the **Install** button. Once the installation has been completed, you will be redirected to the Microweber dashboard.

Reference: [Microweber Documentation](http://docs.microweber.com/).
