### DokuWiki on Ubuntu 18.04

> My test platform: Ubuntu Server 18.04.2

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### 2. Apache and PHP - installation and configuration

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install apache2 php7.2 libapache2-mod-php7.2 php7.2-json php7.2-common php7.2-mbstring \
                    php7.2-xmlrpc php7.2-soap php7.2-gd php7.2-xml php7.2-intl php7.2-tidy php7.2-mysql php7.2-cli \
                    php7.2-ldap php7.2-pdo php7.2-zip php7.2-curl php7.2-sqlite3 unzip wget git -y
```

Once the installation has been completed, edit */etc/php/7.2/apache2/php.ini* and set:

```
...
memory_limit = 256M
...
upload_max_filesize = 100M
...
max_execution_time = 360
...
date.timezone = Europe/Warsaw
...
```

**date.timezone* - yours timezone

Restart the Apache service. On Ubuntu, the services should be started automatically after packages pre-configuration is complete. You can check it and enable the start of services together with the system.

```
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
$ sudo systemctl enable apache2.service
```

#### 3. DokuWiki - installation and configuration

```
$ cd /var/wwww/html/
$ sudo rm -rf index.html
$ sudo git clone --branch stable https://github.com/splitbrain/dokuwiki.git
$ cd dokuwiki
$ sudo cp -R . ../
$ sudo chown -R www-data: /var/www/html/*
$ sudo chmod -R 755 /var/www/html/*
```

Next, create an Apache virtual host file */etc/apache2/sites-available/dokuwiki.conf*:

```
<VirtualHost *:80>
     ServerAdmin admin@local.lnxorg
     DocumentRoot /var/www/html
     ServerName ctx04vm.local.lnxorg

     <Directory /var/www/html/>
          Options FollowSymlinks
          AllowOverride All
          Require all granted
     </Directory>

     ErrorLog ${APACHE_LOG_DIR}/doku_error.log
     CustomLog ${APACHE_LOG_DIR}/doku_access.log combined

     <Directory /var/www/html/>
            RewriteEngine on
            RewriteBase /
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*) index.php [PT,L]
    </Directory>
</VirtualHost>
```

Replace *ServerAdmin* and *ServerName* with parameters corresponding to your case. Save the file and enable the virtual host, the Apache rewrite module and restart web server to apply all the changes:

```
$ sudo a2ensite octobercms
$ sudo a2dissite 000-default
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
```

Open your web browser and type the URL of your DokuWiki website. 
- Provide all the required information like:
    - superuser name,
    - real name,
    - email,
    - password,
    - choose your language and choose the license for yours content. Click on the **Save** button. 
- After the installation has been successfully completed, click the **your new DokuWiki** link. 
- Click on the **Login** button. Provide your *admin username* and *password*, then click on the **Login** button. You will be redirected to the DokuWiki dashboard.

Reference: [DokuWiki Installation](https://www.dokuwiki.org/install).
