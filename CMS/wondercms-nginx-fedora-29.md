### WonderCMS on Fedora 29/30

> My test platform: Fedora 29, Fedora 30 and WonderCMS 2.7.0

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### Requirements for installing and running WonderCMS

- PHP version 7.1 or greater with the curl, mbstring and zip extensions.
- Web server (Apache with mod_rewrite module enabled, Nginx, IIS).

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo dnf update -y
$ sudo dnf install -y wget vim git unzip mc curl 
```

#### 2. Nginx with PHP - installation and configuration

```
$ sudo dnf install -y nginx php-cli php-fpm php-common php-curl php-zip php-mbstring
$ sudo systemctl start nginx.service
$ systemctl status nginx.service
$ sudo systemctl enable nginx.service
$ sudo systemctl start php-fpm.service
$ systemctl status php-fpm.service
$ sudo systemctl enable php-fpm.service
```

Create the virtual host file */etc/nginx/conf.d/wondercms.conf*. For the purposes of this tutorial, I will use the *ctx09vm.local.lnxorg* domain, you can change it to the domain you want to use.

```
server {

  listen 80;

  server_name ctx09vm.local.lnxorg;
  root /var/www/wondercms;

  index index.php;

  location / {
    if (!-e $request_filename) {
      rewrite ^/(.+)$ /index.php?page=$1 last;
    }
  }
  location ~ database.js {
    return 403;
  }

  location ~ \.php(/|$) {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/var/run/php/php-fpm.sock;
  }

}
```

Reload the Nginx configuration:

```
$ sudo nginx -t
$ sudo systemctl reload nginx.service
$ systemctl status nginx.service
```

#### 3. WonderCMS - installation:

```
$ sudo mkdir -p /var/www/wondercms
$ cd /var/www/wondercms
$ sudo wget https://github.com/robiso/wondercms/releases/download/2.7.0/WonderCMS-2.7.0.zip
$ sudo unzip WonderCMS-2.7.0.zip
$ sudo rm WonderCMS-2.7.0.zip
$ cd wondercms
$ sudo cp -r . ../
$ cd ../
$ sudo rm -rf wondercms
$ sudo chown nginx: /var/www/wondercms/ -R
```

Edit */etc/php-fpm.d/www.conf* and set the user and group to *nginx*. Create */var/lib/php/session/* directory and change ownership to *nginx*:

```
$ sudo mkdir -p /var/lib/php/session/ && sudo chown -R nginx: /var/lib/php/session/
```

Restart the PHP-FPM service.

```
$ sudo systemctl restart php-fpm.service
```

Open the URL of your WonderCMS in your web browser and log in with the default password *admin*. Then change them.

Reference: 
- [WonderCMS](https://www.wondercms.com/)
- [Github](https://github.com/robiso/wondercms)
