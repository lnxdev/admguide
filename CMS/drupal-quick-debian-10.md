### Drupal on Debian 10 - quick install

> My test platform: Debian 10.6

#### Requirements

- User with *root* privileges or non-root user with *sudo* privileges. 

#### Apache, PHP and MariaDB installation

```shell
$ sudo apt update && sudo apt upgrade -y          # Reboot if required

$ sudo apt install apache2 mariadb-server mariadb-client php libapache2-mod-php php-cli php-fpm \
                    php-json php-common php-mysql php-zip php-gd php-intl php-mbstring php-curl php-xml php-pear php-tidy php-soap \
                    php-bcmath php-xmlrpc

$ sudo a2enmod rewrite
$ sudo systemctl restart apache2.service
$ systemctl status apache2.service

$ sudo mysql_secure_installation

    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

#### Create a Drupal database

Log in to MariaDB shell, create a database and user for Drupal. Grant privileges to the database. In this example:

- **drupaldb** - database for Drupal,
- **drupalusr** - user for Drupal database,
- **pass4drupal** - secure password for **drupalusr** and **drupaldb**.

```shell
$ mysql -u root -p
Enter password:
MariaDB [(none)]> CREATE DATABASE drupaldb;
MariaDB [(none)]> CREATE USER drupalusr;
MariaDB [(none)]> GRANT ALL PRIVILEGES ON drupaldb.* TO 'drupalusr'@'localhost' IDENTIFIED BY 'pass4drupal';
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT
```

#### Download and install

```shell
$ mkdir download && cd download
$ wget https://www.drupal.org/download-latest/tar.gz -O drupal.tar.gz
$ tar -xvf drupal.tar.gz
$ cd drupal*
$ sudo mv * /var/www/html/
$ sudo chown -R www-data: /var/www/html/*
```

#### Drupal configuration

- Open your web browser and type the URL of your Drupal website.
- Choose the language settings. Click on the **Save and continue** button.
- Select the first option which is the **Standard** profile, and hit the **Save and continue** button.
- Provide your database details like *database name*, *database username*, and *password*. Click on the **Save and continue** button.
- Give your site a name, create administrative username and password, enter your email address, etc.
- Drupal will be installed and you will see a Drupal dashboard.