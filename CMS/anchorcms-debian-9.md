### Anchor CMS on Debian 9

> My test platform: Debian 9.8

Anchor CMS is a lightweight blog system written in PHP. Anchor's source code is hosted on [GitHub](https://github.com/anchorcms/anchor-cms). 

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install -y curl wget vim git unzip mc apt-transport-https lsb-release ca-certificates
```

#### 2. PHP installation

Anchor CMS requires PHP 7.1 or greater with the necessary extensions:

```
$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
$ echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
$ sudo apt update
$ sudo apt install -y php7.2 php7.2-cli php7.2-fpm php7.2-common php7.2-mbstring php7.2-curl php7.2-mysql php7.2-sqlite3 php7.2-gd php7.2-xml
```

Check the PHP version:

```
$ php --version
PHP 7.2.16-1+0~20190307202415.17+stretch~1.gbpa7be82 (cli) (built: Mar  7 2019 20:24:15) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.16-1+0~20190307202415.17+stretch~1.gbpa7be82, Copyright (c) 1999-2018, by Zend Technologies
```

#### 3. MariaDB - installation and configuration

```
$ sudo apt install -y mariadb-server
```

The service should be started automatically after packages pre-configuration is complete. You can check it and enable the start of service together with the system.

```
$ systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

To secure MariaDB installation, use the command below:

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 3
Server version: 10.1.37-MariaDB-0+deb9u1 Debian 9.6

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [mysql]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> exit
Bye
```

Restart MariaDB service:

```
$ sudo systemctl restart mariadb.service
$ systemctl status mariadb.service
```

Log in to MariaDB shell, create a database and user for Anchor CMS. Grant privileges to the database. In this example:

- **anchordb** - database for Anchor CMS
- **anchorusr** - user for Anchor CMS database
- **pass4anchordb** - secure password for anchorusr and anchordb

```
$ mysql -u root -p
Enter password
MariaDB [(none)]> CREATE DATABASE anchordb;
MariaDB [(none)]> CREATE USER 'anchorusr'@'localhost' IDENTIFIED BY 'pass4anchordb';
MariaDB [(none)]> GRANT ALL ON anchordb.* TO 'anchorusr'@'localhost';
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```

#### 4. Nginx installation

```
$ sudo apt install -y nginx
```

When the installation has finished, enable the service so it will be available after each system boot:

```
$ systemctl status nginx.service
$ sudo systemctl enable nginx.service
```

Create a Nginx virtual host file */etc/nginx/sites-available/anchorcms.conf*. For the purpose of this tutorial, I will use *local.lnx* domain, you can change it with the domain you wish to use.

```
server {
    listen 80;

    server_name local.lnx;
    root /var/www/anchorcms;

    index index.php index.html;

    location / {
        try_files $uri $uri/ /index.php;
    }
    
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        include fastcgi_params;
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

}
```

Activate the new *anchorcms.conf* configuration:

```
$ sudo ln -s /etc/nginx/sites-available/anchorcms.conf /etc/nginx/sites-enabled
```

Check Nginx configuration and restart service:

```
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ systemctl status nginx.service
```

#### 5. Composer installation

To install Anchor CMS, you must install Composer:

```
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('SHA384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
$ sudo mv composer.phar /usr/local/bin/composer
```

Check the Composer version:

```
$ composer --version
Composer version 1.8.5 2019-04-09 17:46:47
```

#### 6. Download and install Anchor CMS

Create a document root directory and change ownership to your user:

```
$ sudo mkdir -p /var/www/anchorcms
$ sudo chown -R [your_user]: /var/www/anchorcms
$ cd /var/www/anchorcms
```

Update Composer:

```
$ composer update
Loading composer repositories with package information
Updating dependencies (including require-dev)
Package operations: 0 installs, 4 updates, 0 removals
  - Updating psr/log (1.0.2 => 1.1.0): Downloading (100%)
  - Updating symfony/debug (v4.0.5 => v4.2.5): Downloading (100%)
  - Updating symfony/polyfill-mbstring (v1.7.0 => v1.11.0): Downloading (100%)
  - Updating symfony/console (v3.4.5 => v3.4.24): Downloading (100%)
Writing lock file
Generating autoload files
```

Download Anchor CMS by using Composer:

```
$ composer create-project anchorcms/anchor-cms .
Installing anchorcms/anchor-cms (0.12.7)
  - Installing anchorcms/anchor-cms (0.12.7): Downloading (100%)
Created project in .
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Warning: The lock file is not up to date with the latest changes in composer.json. You may be getting outdated dependencies. Run update to update them.
Package operations: 13 installs, 0 updates, 0 removals
  - Installing indigophp/hash-compat (v1.1.0): Downloading (100%)
  - Installing ircmaxell/password-compat (v1.0.4): Downloading (100%)
  - Installing ircmaxell/security-lib (v1.1.0): Downloading (100%)
  - Installing ircmaxell/random-lib (v1.2.0): Downloading (100%)
  - Installing peridot-php/leo (1.6.1): Downloading (100%)
  - Installing symfony/polyfill-mbstring (v1.7.0): Downloading (100%)
  - Installing psr/log (1.0.2): Downloading (100%)
  - Installing symfony/debug (v4.0.5): Downloading (100%)
  - Installing symfony/console (v3.4.5): Downloading (100%)
  - Installing phpunit/php-timer (1.0.9): Downloading (100%)
  - Installing peridot-php/peridot-scope (1.3.0): Downloading (100%)
  - Installing evenement/evenement (v2.1.0): Downloading (100%)
  - Installing peridot-php/peridot (1.19.0): Downloading (100%)
symfony/console suggests installing symfony/event-dispatcher
symfony/console suggests installing symfony/lock
symfony/console suggests installing symfony/process
Generating autoload files
```

Change ownership to *www-data*:

```
$ sudo chown -R www-data: /var/www/anchorcms
$ sudo mkdir -p /var/lib/php/session && sudo chown -R www-data: /var/lib/php
```

- Open your web browser and type the URL of your Anchor CMS website.
- Click on the **Run the installer** button.
- Choose the *language settings* and *timezone*. Click on the **Next Step** button.
- Provide your database details like *database name*, *database username*, and *password*. Click on the **Next Step** button.
- Give your site a name and description. Click on the **Next Step** button.
- Create administrative *username* and *password*, enter your *email address*. Click on the **Complete** button.

After installation, remove the installation folder for security reasons.

```
$ sudo rm -rf /var/www/anchorcms/install
```

Reference: 

- [Anchor CMS Docs](https://docs.anchorcms.com/)
- [Translations for Anchor CMS](https://github.com/anchorcms/anchor-translations)
- [GitHub](https://github.com/anchorcms/anchor-cms)
