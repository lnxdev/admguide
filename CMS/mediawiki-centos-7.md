### MediaWiki on CentOS 7

> My test platform: CentOS Linux release 7.6.1810 (Core) and MediaWiki 1.32.0

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### 2. Enable the epel and remi repositories.

```
$ sudo yum -y update
$ sudo yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
$ sudo yum -y install epel-release
```

I am going to use php7.3, I will need to disable the installation of php5.4:

```
$ sudo yum -y install yum-utils
$ sudo yum-config-manager --disable remi-php54
$ sudo yum-config-manager --enable remi-php73
```

#### 3. Installation Apache, MariaDB and PHP with necessary extensions. 

```
$ sudo yum -y update
$ sudo yum -y install httpd mariadb-server mariadb-client php php-mysql php-pdo php-gd php-mbstring php-xml php-intl texlive git
```

Start and enable the services with:

```
$ sudo systemctl start httpd.service
$ sudo systemctl status httpd.service 
$ sudo systemctl enable httpd.service
$ sudo systemctl start mariadb.service
$ sudo systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

#### 4. MariaDB configuration

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell, create a database and user for MediaWiki. Grant privileges to the database. In this example:

- **mediawikidb** - database for MediaWiki
- **mediawikiusr** - user for MediaWiki database
- **pass4mediawiki** - secure password for mediawikiusr and mediawikidb

```
$ mysql -u root -p
Enter password:
MariaDB [(none)]> CREATE DATABASE mediawikidb;
MariaDB [(none)]> CREATE USER 'mediawikiusr'@'localhost' identified by 'pass4mediawiki';
MariaDB [(none)]> GRANT ALL PRIVILEGES on mediawikidb.* to 'mediawikiusr'@'localhost';
MariaDB [(none)]> exit;
```

#### 5. Download and installation MediaWiki

To download the latest package, head to [MediaWiki Download page](https://www.mediawiki.org/wiki/Download/). At the time of writing this tutorial, the latest version of MediaWiki is 1.32.0.

```
$ cd /tmp
$ wget https://releases.wikimedia.org/mediawiki/1.32/mediawiki-1.32.0.tar.gz
$ tar -xf mediawiki-1.32.0.tar.gz
$ sudo mv mediawiki-1.32.0/* /var/www/html/
```

#### 6. Firewall configuration

```
$ sudo firewall-cmd --permanent --add-port=80/tcp
$ sudo firewall-cmd --permanent --add-port=443/tcp
$ sudo firewall-cmd --reload
```

#### 7. MediaWiki configuration

- Open your web browser and type the URL of your MediaWiki website. 
- Choose the language settings. Click on the **Continue** button.
- Check MediaWiki Requirements. Click on the **Continue** button.
- Provide your database details like *database name*, *database username*, and *password*. Click on the **Continue** button.
- Give your wiki a name, create administrative username and password. Click on the **Continue** button.
- On the next screens, you can leave the default settings, unless you want to make any other custom changes.
- You will be provided with a file called *LocalSettings.php*. You will have to place that file in the directory root for your wiki.
- You have successfully installed MediaWiki. Type the URL of your MediaWiki website. You should see the newly installed MediaWiki.

Reference: [MediaWiki Manual:Contents](https://www.mediawiki.org/wiki/Manual:Contents).
