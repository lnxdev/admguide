### Ansible AWX on Ubuntu 18.04 LTS

> My test platform: Ubuntu Server 18.04.2 LTS and AWX 4.0.0

The AWX Project is an open source community project, that enables users to better control their Ansible project use in IT environments. 

#### 1. Requirements

- RAM memory +4GB and 2 cores
- User with root privileges or non-root user with sudo privileges.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install -y wget vim git unzip mc
```

#### 2. Install Docker and Ansible

```
$ sudo apt install ansible docker.io -y
$ docker --version && ansible --version
```

#### 3. Install additional packages for AWX installation

```
$ sudo apt install python-pip -y
$ pip install docker-compose==1.9.0
$ pip install docker-py
$ sudo apt install nodejs npm -y
$ sudo npm install npm --global
```

#### 4. Download and configure the AWX source code

Download the AWX source code from the git repository:

```
$ git clone https://github.com/ansible/awx.git
$ cd awx/installer/
```

Generate a new secret key for AWX using the *openssl* command and save it for later:

```
$ openssl rand -hex 32
```

Now edit the *inventory* configuration file and change:

- **postgres_data_dir** to the */var/lib/pgdocker* directory,
- **host_port** to *8080*,
- **docker_compose_dir** to the */var/lib/awx* directory,
- the credentials for the **pg_password**, **rabbitmq_password**, **rabbitmq_erlang_cookie**, **admin_user** and **admin_password** with your own password credentials,
- **secret_key** using the secret key generated above,
- add **use_docker_compose** and set the value to *true*, 
- uncomment the **project_data_dir** and leave the value as default.

Contents of the sample *inventory* file:

```
localhost ansible_connection=local ansible_python_interpreter="/usr/bin/env python"

[all:vars]

dockerhub_base=ansible

awx_task_hostname=awx
awx_web_hostname=awxweb
postgres_data_dir=/var/lib/pgdocker
host_port=8080
docker_compose_dir=/var/lib/awx

pg_username=awx
pg_password=awxpgpass
pg_database=awxdb
pg_port=5432

rabbitmq_password=awxrabitpass
rabbitmq_erlang_cookie=cookiepass

admin_user=sysadmin
admin_password=serwis

create_preload_data=True

secret_key=32a75bd0a57567d7f935d1a675db7d590a8d949191ba477197228b95f7f04b78

project_data_dir=/var/lib/awx/projects

use_docker_compose=true
```

#### 5. Ansible AWX installation

Install the awx using ansible-playbook command. The playbook will do some tasks including downloading docker images and creating new containers:

```
$ sudo ansible-playbook -i inventory install.yml
```

The docker-compose configuration is located in the */var/lib/awx/docker-compose.yml* file. Check all available docker containers and logs of the **task** service using the **docker-compose** command. 

```
$ cd /var/lib/awx/
$ sudo ~/.local/bin/docker-compose ps
$ sudo ~/.local/bin/docker-compose logs task
```

#### 6. Testing

Open your web browser and type the URL of your AWX website *http://ip-address:8080/*. Type the username and password, click the **SIGN IN** button.

Reference: [AWX](https://github.com/ansible/awx).

---

### Update 1 - Nginx as a reverse proxy for AWX

#### Nginx installation:

```
$ sudo apt install nginx -y
$ cd /etc/nginx/sites-available/
$ sudo vim awx.conf
```

**a)** http only:

```
server {
   listen 80;
   server_name ctx01vm.local.lnx;
   access_log /var/log/nginx/awx.access.log;
   error_log /var/log/nginx/awx.error.log;
   
location / {
    proxy_http_version 1.1;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_pass http://192.168.1.181:8080/;
    }
}    
```

**b)** http and https:

```
server {
   listen 80;
   server_name ctx01vm.local.lnx;
   add_header Strict-Transport-Security max-age=2592000;
   rewrite ^ https://$server_name$request_uri? permanent;
}

server {
    listen 443 ssl http2;
    server_name ctx01vm.local.lnx;

    access_log /var/log/nginx/awx.access.log;
    error_log /var/log/nginx/awx.error.log;

    ssl on;
    ssl_certificate /etc/nginx/ssl/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/privkey.pem;
    ssl_session_timeout 5m;
    ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;
    ssl_protocols TLSv1.2;
    ssl_prefer_server_ciphers on;

location / {
    proxy_http_version 1.1;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_pass http://192.168.1.181:8080/;
    }
}
```

**proxy_pass* - the URL of your AWX website

Activate the *awx* virtual host and test the nginx configuration:

```
$ sudo ln -s /etc/nginx/sites-available/awx.conf /etc/nginx/sites-enabled/
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ sudo systemctl status nginx.service
```
