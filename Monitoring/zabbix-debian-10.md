### Zabbix on Debian 10

> My test platform: Debian 10 and grml64 2018.12

#### 1. Requirements

- User with root privileges.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
# apt update && apt upgrade -y
# apt install -y wget vim git unzip mc curl net-tools gnupg
```

#### 2. Installing Apache Web Server and PHP packages

```
# apt install apache2 php php-mysql php-mysqlnd php-ldap php-bcmath php-mbstring php-gd php-pdo php-xml libapache2-mod-php
```

On Debian 10, services should be started and turned on automatically after packages pre-configuration is complete, so there is no need to start and enable them manually. You can check if it is up and running:

```
# systemctl status apache2.service
```

#### 3. MySQL installation and configuration

```
# cd /tmp
# wget https://dev.mysql.com/get/mysql-apt-config_0.8.13-1_all.deb
# dpkg -i mysql-apt-config_0.8.13-1_all.deb
# apt update -y
# apt install mysql-server
# systemctl status mysql
# mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

MySQL installation test:

```
# mysqladmin -u root -p version
Enter password:
mysqladmin  Ver 8.0.17 for Linux on x86_64 (MySQL Community Server - GPL)
Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Server version          8.0.17
Protocol version        10
Connection              Localhost via UNIX socket
UNIX socket             /var/run/mysqld/mysqld.sock
Uptime:                 8 min 15 sec

Threads: 27  Questions: 4535  Slow queries: 0  Opens: 372  Flush tables: 3  Open tables: 292  Queries per second avg: 9.161
```

Log in to MySQL shell, create a database and user for Zabbix. Grant privileges to the database. In this example:

- **zabbixdb** - database for Zabbix
- **zabbixusr** - user for Zabbix database
- **pass4zabbixdb** - secure password for zabbixusr and zabbixdb

```
# mysql -u root -p
Enter password:
mysql> CREATE DATABASE zabbixdb;
mysql> CREATE USER 'zabbixusr'@'localhost' IDENTIFIED BY 'pass4zabbixdb';
mysql> GRANT ALL ON zabbixdb.* TO 'zabbixusr'@'localhost';
mysql> FLUSH PRIVILEGES;
mysql> ALTER USER 'zabbixusr'@'localhost' IDENTIFIED WITH mysql_native_password by 'pass4zabbixdb';
mysql> EXIT;
```

#### 4. Zabbix Server installation and configuration

```
# cd /tmp
# wget https://repo.zabbix.com/zabbix/4.2/debian/pool/main/z/zabbix-release/zabbix-release_4.2-2+buster_all.deb
# dpkg -i zabbix-release_4.2-2+buster_all.deb
# apt update
# apt install -y zabbix-server-mysql zabbix-frontend-php zabbix-agent
# zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -u zabbixusr -p zabbixdb
```

For the Zabbix server daemon to use the database created for it in the */etc/zabbix/zabbix_server.conf* file, set:

```
[...]
DBHost=localhost
[...]
DBName=zabbixdb
[...]
DBUser=zabbixusr
[...]
DBPassword=pass4zabbixdb
[...]
```

Define your time zone in the */etc/zabbix/apache.conf* file:

```
[...]
    <IfModule mod_php7.c>
        [...]
        php_value date.timezone Europe/Warsaw
    </IfModule>
[...]
```

Restart Apache2 service:

```
# systemctl restart apache2.service
# systemctl status apache2.service
```

Start Zabbix Server and Zabbix Agent:

```
# systemctl start zabbix-server.service zabbix-agent.service
# systemctl status zabbix-server.service zabbix-agent.service
# systemctl enable zabbix-server.service zabbix-agent.service
```

If you have the *UFW* firewall service running, you need to open the port *80(HTTP)* and *443(HTTPS)* to allows traffic to the Apache server.

#### 4.1 Configuring Zabbix Web Frontend Interface

- Open your web browser and type the URL of your Zabbix setup interface *http://SERVER_IP/zabbix/setup.php*
- Click on the **Next step** button.
- The installer will check the pre-requisites, if all required PHP modules and configuration options are OK, click **Next step** button to proceed.
- Provide your database details like *database type*, *database host*, *database name*, *database username*, and *password*. Click on the **Next step** button.
- Set a name for the installation. Click on the **Next step** button.
- Installer should show you the pre-installation summary page. If all is fine, click **Next step** to complete the setup.
-  Click on the **Finish** button.
- To login, enter the username *Admin* and password *zabbix*.

Reference: [Zabbix Documentation](https://www.zabbix.com/manuals)
