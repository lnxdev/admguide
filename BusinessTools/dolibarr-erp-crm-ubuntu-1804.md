### Basic installation of Dolibarr ERP & CRM on Ubuntu 18.04.1 LTS

> My test platform: Ubuntu Server 18.04.1 LTS and Dolibarr 8.0.4 

#### 1. Requirements

Ubuntu Server 18.04 and a non-root user with sudo privileges.

#### 2. Installation Apache, PHP and MariaDB

```
$ sudo apt update
$ sudo apt install apache2 mariadb-server php7.2 libapache2-mod-php7.2 php7.2-common php7.2-curl php7.2-intl php7.2-mbstring \
                    php7.2-json php7.2-xmlrpc php7.2-soap php7.2-mysql php7.2-gd php7.2-xml php7.2-cli php7.2-zip \
                    wget unzip -y
```

Once the installation has been completed, edit */etc/php/7.2/apache2/php.ini* and set:

```
...
memory_limit = 512M
...
upload_max_filesize = 150M
...
max_execution_time = 360
...
date.timezone = Europe/Warsaw
...
```

**date.timezone* - yours timezone

Restart Apache service. On Ubuntu, the Apache and MariaDB should be started automatically immediately after packages pre-configuration is complete, you can simply enable it.

```
$ sudo systemctl restart apache2.service
$ sudo systemctl enable apache2.service
$ sudo systemctl status apache2.service
$ sudo systemctl enable mariadb.service
$ sudo systemctl status mariadb.service
```

#### 3. MariaDB configuration

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below and set root password for MariaDB:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 35
Server version: 10.1.34-MariaDB-0ubuntu0.18.04.1 Ubuntu 18.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [(none)]> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
Query OK, 1 row affected (0.03 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [mysql]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> exit
Bye
```

Restart MariaDB service:

```
$ sudo systemctl restart mariadb.service
$ sudo systemctl status mariadb.service
```

Log in to MariaDB shell, create a database and user for Dolibarr. Grant privileges to the database. In this example:

- **dolibarrdb** - database for Dolibarr
- **dolibarrusr** - user for Dolibarr database
- **pass4dolibarr** - secure password for dolibarrusr and dolibarrdb

```
$ mysql -u root -p
Enter password:
MariaDB [(none)]> CREATE DATABASE dolibarrdb character set UTF8 collate utf8_bin;
MariaDB [(none)]> CREATE USER dolibarrusr;
MariaDB [(none)]> GRANT ALL PRIVILEGES ON dolibarrdb.* TO 'dolibarrusr'@'localhost' IDENTIFIED BY 'pass4dolibarr';
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT
```

#### 4. Download and installation Dolibarr

```
$ cd /tmp
$ wget https://sourceforge.net/projects/dolibarr/files/Dolibarr%20ERP-CRM/8.0.4/dolibarr-8.0.4.zip
$ unzip dolibarr-8.0.4.zip
$ sudo mkdir /var/www/html/dolibarr
$ sudo cp -r dolibarr-8.0.4/htdocs/* /var/www/html/dolibarr/
$ sudo chown -R www-data:www-data /var/www/html/dolibarr/
$ sudo chmod -R 755 /var/www/html/dolibarr/
```

Create a folder for Dolibarr to store uploaded documents:

```
$ mkdir /var/documents
$ chown www-data:www-data /var/documents
$ chmod 700 /var/documents
```

Next, create an Apache virtual host file */etc/apache2/sites-available/dolibarr.conf*:

```
<VirtualHost *:80>
     ServerAdmin admin@local.lnxorg
     DocumentRoot /var/www/html/dolibarr
     ServerName ctx09vm.local.lnxorg

     <Directory /var/www/html/dolibarr>
          Options +FollowSymlinks
          AllowOverride All
          Require all granted
     </Directory>

     ErrorLog ${APACHE_LOG_DIR}/dolibarr_error.log
     CustomLog ${APACHE_LOG_DIR}/dolibarr_access.log combined
</VirtualHost>
```

Replace **ServerAdmin** and **ServerName** with parameters corresponding to your case. Save the file and enable apache virtual host:

```
$ sudo a2ensite dolibarr
```

Next, enable Apache rewrite module and reload apache service with the following command:

```
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
```

#### 5. Dolibarr configuration

- Open your web browser and type the URL of your Dolibarr website. 
- Select your language and click on the **Next step** button. 
- Validate the PHP checks and click on the **Start** button. 
- Provide your document directory (*/var/documents*) and the database details and click on the **Next step** button. 
- Click on the **Next step** button. 
- Installation successful, click on the **Next step** button. 
- Set a new admin username and password. Click on the **Next step** button. 
- Click on the **Go to Dolibarr** button. 
- Enter your admin username and password to log in.
- Click on **Company/Organization** and enter the *Details* of your company. Go to **Modules/Applications** and select which *Modules* you want to use.

To finalize the installation and remove the installation warnings on the dashboard:

```
$ sudo touch /var/documents/install.lock
$ sudo chown root:root /var/www/html/dolibarr/conf/conf.php
```
