### Metabase Business Analytics on Ubuntu 20.04

> My test platform: Ubuntu Server 20.04.1

#### 1. Requirements

- User with *root* privileges or non-root user with *sudo* privileges.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo apt update; sudo apt upgrade -y          # Reboot if required
$ sudo apt install mc nano vim wget curl git tree unzip -y
```

#### 2. Java, MariaDB - installation and configuration

```
$ sudo apt install openjdk-11-jdk openjdk-11-jre mariadb-server -y
$ java -version
$ sudo systemctl is-active mariadb.service
$ sudo systemctl is-enabled mariadb.service
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell, create a database and user for Metabase. Grant privileges to the database. In this example:

- **metabasedb** - database for Metabase,
- **metabaseusr** - user for Metabase database,
- **pass4metabasedb** - secure password for metabaseusr and metabasedb.

```
$ sudo mysql
Enter password:

MariaDB [(none)]> CREATE DATABASE metabasedb;
MariaDB [(none)]> CREATE USER 'metabaseusr'@'localhost' IDENTIFIED BY 'pass4metabasedb';
MariaDB [(none)]> GRANT ALL PRIVILEGES ON metabasedb.* TO 'metabaseusr'@'localhost';
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```

#### 3. Metabase installation

```
$ wget https://downloads.metabase.com/v0.36.2/metabase.jar
$ sudo mkdir /opt/metabase
$ sudo mv metabase.jar /opt/metabase/
$ sudo addgroup --quiet --system metabase
$ sudo adduser --quiet --system --ingroup metabase --no-create-home --disabled-password metabase
$ sudo chown -R metabase: /opt/metabase
$ sudo chmod -R 755 /opt/metabase
```

Copy the sample *metabase.service* file to */etc/systemd/system/metabase.service*:

```
# metabase.service

[Unit]
Description=Metabase server

[Service]
WorkingDirectory=/opt/metabase/
ExecStart=/usr/bin/java -jar /opt/metabase/metabase.jar
User=metabase
Type=simple
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```

Reload the systemd daemon, start the Metabase service and enable it to start at system reboot:

```
$ sudo systemctl daemon-reload
$ sudo systemctl start metabase.service
$ sudo systemctl enable metabase.service
$ systemctl status metabase.service
```

#### 4. Nginx as a reverse proxy for Metabase

```
$ sudo apt install nginx -y
```

Create a new Nginx virtual host configuration file */etc/nginx/sites-available/metabase.conf*. Add the following lines:

```
upstream metabase {
  server 127.0.0.1:3000;
}

server {
    listen 80;
    server_name ctx08vm.lnxorg.local;
    access_log /var/log/nginx/metabase-access.log;
    error_log /var/log/nginx/metabase-error.log;

    location / {
        proxy_pass http://metabase/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $http_host;

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forward-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forward-Proto http;
        proxy_set_header X-Nginx-Proxy true;

        proxy_redirect off;
    }
}
```

Replace **ctx08vm.lnxorg.local** with domain name corresponding to your case. 

Activate the Nginx virtual host, test configuration and restart the Nginx service to apply the changes:

```
$ sudo ln -s /etc/nginx/sites-available/metabase.conf /etc/nginx/sites-enabled/
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ systemctl status nginx.service
```

#### 5. Metabase Web Interface

- Open your web browser and type the URL of your Metabase. Click on the **Let's get started** button. 
- Select your language and click on the **Next** button.
- Provide your name, email address, password and company or team name. Click on the **Next** button.
- Provide your database details like database type, database name, database username, port and password. Click on the **Next** button.
- Enable your desired option in **Usage data preferences** and click on the **Next** button.
- Click on the **Take me to Metabase**. You should see the Metabase dashboard.

Reference: [Metabase Documentation](https://www.metabase.com/docs/latest/).