### vTiger CRM on Ubuntu 18.10

> My test platform: Ubuntu Server 18.10 and vTiger CRM 7.1.0

#### 1. Requirements

- A non-root user with sudo privileges.

#### 2. Installation Apache, PHP and MariaDB

```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install apache2 mariadb-server libapache2-mod-php7.2 php7.2 php7.2-cli php7.2-mysql php7.2-common php7.2-zip \
                    php7.2-mbstring php7.2-xmlrpc php7.2-curl php7.2-soap php7.2-gd php7.2-xml php7.2-intl php7.2-ldap \
                    php7.2-imap unzip wget -y
```

Once the installation has been completed, edit */etc/php/7.2/apache2/php.ini* and set:

```
...
file_uploads = On
...
allow_url_fopen = On
...
memory_limit = 256M
...
upload_max_filesize = 30M
...
post_max_size = 40M
...
max_execution_time = 60
...
max_input_vars = 1500
...
date.timezone = Europe/Warsaw
...
```

**date.timezone* - yours timezone

Restart Apache service. On Ubuntu, the services should be started automatically after packages pre-configuration is complete. You can check it and enable services together with the system.

```
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
$ sudo systemctl enable apache2.service
$ sudo systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

#### 3. MariaDB configuration

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 31
Server version: 10.1.29-MariaDB-6ubuntu2 Ubuntu 18.10

Copyright (c) 2000, 2017, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [mysql]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> exit
Bye
```

Restart MariaDB service:

```
$ sudo systemctl restart mariadb.service
$ sudo systemctl status mariadb.service
```

Log in to MariaDB shell, create a database and user for vTiger CRM. Grant privileges to the database. In my example:

- **vtigerdb** - database for vTiger CRM
- **vtigerusr** - user for vTiger CRM database
- **pass4vtiger** - secure password for vtigerusr and vtigerdb 

```
$ mysql -u root -p
Enter password:
$ MariaDB [(none)]> CREATE DATABASE vtigerdb;
$ MariaDB [(none)]> CREATE USER 'vtigerusr'@'localhost' IDENTIFIED BY 'pass4vtiger';
$ MariaDB [(none)]> GRANT ALL PRIVILEGES ON vtigerdb.* TO 'vtigerusr'@'localhost' IDENTIFIED BY 'pass4vtiger' WITH GRANT OPTION;
$ MariaDB [(none)]> ALTER DATABASE vtigerdb CHARACTER SET utf8 COLLATE utf8_general_ci;
$ MariaDB [(none)]> FLUSH PRIVILEGES;
$ MariaDB [(none)]> exit
```

#### 4. Download and installation vTiger CRM

To download the latest package, head to [vTiger CRM download page](https://www.vtiger.com/open-source-crm/download-open-source/). At the time of writing this tutorial, the latest version of vTiger CRM is 7.1.0.

```
cd /tmp
$ wget https://excellmedia.dl.sourceforge.net/project/vtigercrm/vtiger%20CRM%207.1.0/Core%20Product/vtigercrm7.1.0.tar.gz
$ tar -xvzf vtigercrm7.1.0.tar.gz
$ sudo cp -r vtigercrm /var/www/html/
$ sudo chown -R www-data:www-data /var/www/html/vtigercrm
$ sudo chmod -R 755 /var/www/html/vtigercrm
```

Next, create an Apache virtual host file */etc/apache2/sites-available/vtigercrm.conf*:

```
<VirtualHost *:80>
     ServerAdmin admin@local.lnxorg
     ServerName ctx05vm.local.lnxorg
     DocumentRoot /var/www/html/vtigercrm/

     <Directory /var/www/html/vtigercrm/>
        Options FollowSymlinks
        AllowOverride All
        Require all granted
     </Directory>

     ErrorLog /var/log/apache2/vtigercrm_error.log
     CustomLog /var/log/apache2/vtigercrm_access.log combined
</VirtualHost>
```

Replace **ServerAdmin** and **ServerName** with parameters corresponding to your case. Save the file and enable virtual host, the Apache rewrite module and restart web server to apply all the changes:

```
$ sudo a2ensite vtigercrm
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2.service
$ sudo systemctl status apache2.service
```

#### 5. vTiger CRM configuration

- Open your web browser and type the URL of your vTiger CRM website. 
- Click on the **Install** button.
- Accept the vTiger CRM public licence.
- Verify installation prerequisites. Click on the **Next** button.
- Provide *database name*, *host name*, *database username* and *password* for database. Set a new admin password. Click on the **Next** button.
- Select your industry. Click on the **Next** button.
- Select modules. Click on the **Next** button. 

You have successfully installed vTiger CRM.

Reference: [vTiger CRM Help](https://www.vtiger.com/help/).
