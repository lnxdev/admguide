### Shopware Community Edition (CE) on Centos 7

> My test platform: CentOS Linux release 7.6.1810 (Core) and Shopware 5.x

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
$ sudo yum update -y
$ sudo yum install -y curl wget vim unzip socat epel-release mc
```

#### 2. PHP and necessary PHP extensions installation

```
$ cd /tmp
$ wget https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
$ sudo yum localinstall webstatic-release.rpm
$ sudo yum repolist
$ sudo yum install -y php72w php72w-cli php72w-fpm php72w-common php72w-mysql php72w-curl php72w-json php72w-zip \
                        php72w-gd php72w-xml php72w-mbstring php72w-opcache
```

Start and enable PHP-FPM service:

```
$ sudo systemctl start php-fpm.service
$ systemctl status php-fpm.service
$ sudo systemctl enable php-fpm.service
```

#### 3. IonCube Loader installation (optional)

```
$ cd /tmp && wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
$ tar xfz ioncube_loaders_lin_*.gz
```

Find the PHP extensions directory on the system:

```
$ php -i | grep extension_dir
extension_dir => /usr/lib64/php/modules => /usr/lib64/php/modules
...
```

Copy the IonCube Loader into the PHP extensions directory:

```
sudo cp /tmp/ioncube/ioncube_loader_lin_7.2.so /usr/lib64/php/modules
```

In the */etc/php.ini* file, add the line below to include the IonCube loader. Place it anywhere below the [PHP] line:

```
...
zend_extension = /usr/lib64/php/modules/ioncube_loader_lin_7.2.so
...
```

Save the file and restart PHP-FPM:

```
$ sudo systemctl restart php-fpm.service
$ systemctl status php-fpm.service
```

Check the PHP version. You should see something similar:

```
$ php --version
PHP 7.2.17 (cli) (built: May 13 2019 18:03:04) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with the ionCube PHP Loader (enabled) + Intrusion Protection from ioncube24.com (unconfigured) v10.3.4, Copyright (c) 2002-2019, by ionCube Ltd.
    with Zend OPcache v7.2.17, Copyright (c) 1999-2018, by Zend Technologies
```

#### 4. MariaDB - installation and configuration

```
$ sudo yum install -y mariadb-server
```

Start and enable MariaDB service:

```
$ sudo systemctl start mariadb.service
$ systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

To secure MariaDB installation, use the command below:

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell, create a database and user for Shopware. Grant privileges to the database. In this example:

- *shopwaredb* - database for Shopware
- *shopwareusr* - user for Shopware database
- *pass4shopware* - secure password for shopwareusr and shopwaredb

```
$ mysql -u root -p
Enter password:
MariaDB [(none)]> CREATE DATABASE shopwaredb;
MariaDB [(none)]> CREATE USER 'shopwareusr'@'localhost' identified by 'pass4shopware';
MariaDB [(none)]> GRANT ALL PRIVILEGES on shopwaredb.* to 'shopwareusr'@'localhost';
MariaDB [(none)]> exit;
```

Replace *shopwaredb*, *shopwareusr* and *pass5shopware* with your own names.

#### 5. Nginx - installation and configuration

```
$ sudo yum install -y nginx
```

When the installation has finished, enable the service so it will be available after each system boot:

```
$ sudo systemctl start nginx.service
$ systemctl status nginx.service
$ sudo systemctl enable nginx.service
```

Create the virtual host file */etc/nginx/conf.d/shopware.conf*. For the purposes of this tutorial, I will use the *ctx05vm.local.lnxorg* domain, you can change it to the domain you want to use.

```
server {
    listen 80;

    server_name ctx05vm.local.lnxorg;
    root /var/www/shopware;

    index shopware.php index.php;

    location / {
        try_files $uri $uri/ /shopware.php$is_args$args;
    }

    location /recovery/install {
      index index.php;
      try_files $uri /recovery/install/index.php$is_args$args;
    }

    location ~ \.php$ {
        include fastcgi.conf;
        fastcgi_pass 127.0.0.1:9000;
    }
}
```

Check Nginx configuration and restart service:

```
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ systemctl status nginx.service
```

#### 6. Shopware installation

```
$ sudo mkdir -p /var/www/shopware
$ cd /var/www/shopware
$ sudo wget https://releases.shopware.com/install_5.5.8_d5bf50630eeaacc6679683e0ab0dcba89498be6d.zip?_ga=2.141661361.269357371.1556739808-1418008019.1556603459 -O shopware.zip
```

> NOTE: Update download URL if there is a newer release.

```
$ sudo unzip shopware.zip
$ sudo rm shopware.zip
$ sudo chown -R nginx: /var/www/shopware
```

Edit */etc/php-fpm.d/www.conf* file. Set *user* and *group* to *nginx*:

```
...
user = nginx
...
group = nginx
...
```

Create */var/lib/php/session* directory and change its ownership to a user *nginx*.

```
$ sudo mkdir -p /var/lib/php/session && sudo chown -R nginx: /var/lib/php/session
```

Edit */etc/php.ini* and set:

```
...
memory_limit = 256M
...
upload_max_filesize = 6M
...
allow_url_fopen = On
...
```

Reload *php-fpm.service*:

```
$ sudo systemctl reload php-fpm.service
$ systemctl status php-fpm.service
```

#### 7. Shopware configuration

- Open your web browser and type the URL of your Shopware website.
- Choose the language settings. Click on the **Next** button.
- Check if the Shopware requirements have been met. Click on the **Next** button.
- Accept license. Click on the **Next** button.
- Provide your database details like *database server*, *database name*, *database username*, and *password*. Click on the **Next** button.
- Start the installation to create database tables. Click on the **Start installation** button.
- You will see a message about successful database import. Click on the **Next** button.
- Choose a license (in this example CE). Click on the **Next** button.
- Specify details like:
    - **Name of your shop**,
    - **E-mail address of the shop**,
    - **Main language**,
    - **Default currency**,
    - **Admin name**,
    - **Admin login name**,
    - **Admin e-mail**. Click on the **Next** button.
- Installation is complete. Click on the **Go to shop backend(administration)**.

The backend of Shopware is located at */backend* (in this example: *http://ctx05vm.local.lnxorg/backend*).

Reference:
- [Shopware](https://en.shopware.com/)
- [Github](https://github.com/shopware/shopware)
