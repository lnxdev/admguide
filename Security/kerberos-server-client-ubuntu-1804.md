### Kerberos Server and Client on Ubuntu 18.04 LTS

> My test platform: Ubuntu Server 18.04.2

In this tutorial, I will show you how to set up Kerberos authentication between two Ubuntu 18.04.x servers. I will install and configure the Kerberos server on the Ubuntu server and then install the Kerberos client on another Ubuntu server.  

#### 1. Requirements

- Ubuntu server and client with a non-root user with sudo privileges.

In my example:

|        | ip address    | FQDN                 | 
|--------|---------------|----------------------|
| server | 192.168.1.212 | ctx12vm.local.lnxorg |
| client | 192.168.1.186 | ctx06vm.local.lnxorg |

#### 2. Configuration the FQDN on the server and the client.

- Server:

```
$ echo "192.168.1.212 ctx12vm.local.lnxorg ctx12vm" | sudo tee -a /etc/hosts
$ sudo hostnamectl set-hostname ctx12vm.local.lnxorg
```

- Client:

```
$ echo "192.168.1.186 ctx06vm.local.lnxorg ctx06vm" | sudo tee -a /etc/hosts
$ sudo hostnamectl set-hostname ctx06vm.local.lnxorg
```

#### 3. Installation and configuration KDC Kerberos server

```
$ sudo apt install -y krb5-kdc krb5-admin-server krb5-config
```

During the installation, setup will be asked about the *Kerberos realm*, the *Kerberos server of the realm*, and the *Administrative server*. By default, the Kerberos will use the Kerberos server domain name as a REALM.

In my case:

- *Kerberos realm* - **local.lnxorg**
- *Kerberos server* - **ctx12vm.local.lnxorg**
- *Administrative server* - **ctx12vm.local.lnxorg**

Setting the master password for the Kerberos REALM:

```
$ sudo krb5_newrealm
```

I need to create the administrator user for the KDC Kerberos server (in my case, the admin's name is *root*), add the Kerberos server hostname to the database, and then create the keytab for the Kerberos server.

```
$ sudo kadmin.local
Authenticating as principal root/admin@LOCAL.LNXORG with password.

kadmin.local:  addprinc root/admin
WARNING: no policy specified for root/admin@LOCAL.LNXORG; defaulting to no policy
Enter password for principal "root/admin@LOCAL.LNXORG":
Re-enter password for principal "root/admin@LOCAL.LNXORG":
Principal "root/admin@LOCAL.LNXORG" created.

kadmin.local:  addprinc -randkey host/ctx12vm.local.lnxorg
WARNING: no policy specified for host/ctx12vm.local.lnxorg@LOCAL.LNXORG; defaulting to no policy
Principal "host/ctx12vm.local.lnxorg@LOCAL.LNXORG" created.

kadmin.local:  ktadd host/ctx12vm.local.lnxorg
Entry for principal host/ctx12vm.local.lnxorg with kvno 2, encryption type aes256-cts-hmac-sha1-96 added to keytab FILE:/etc/krb5.keytab.
Entry for principal host/ctx12vm.local.lnxorg with kvno 2, encryption type aes128-cts-hmac-sha1-96 added to keytab FILE:/etc/krb5.keytab.

kadmin.local: quit
```

I need to add the **root/admin** principle to the access control list by editing the */etc/krb5kdc/kadm5.acl* file and restart the service.

```
$ echo "root/admin *" | sudo tee -a /etc/krb5kdc/kadm5.acl
sudo systemctl restart krb5-admin-server.service
sudo systemctl status krb5-admin-server.service
```

#### 4. Installation and configuration Kerberos client

```
sudo apt install -y krb5-user libpam-krb5 libpam-ccreds auth-client-config
```

During the installation, setup will be asked about the *Kerberos Realm*, the *Kerberos server of the Realm*, and the *Administrative server*. The answers are the same as for the server installation.

I connect to the KDC Kerberos server using the **kadmin** command and password for **root/admin** principle. I need to add the client's FQDN to the Kerberos database and the keytab file for the client.

```
$ sudo kadmin
addprinc -randkey host/ctx06vm.local.lnxorg
ktadd host/ctx06vm.local.lnxorg
quit
```

#### 5. Tests

I am going to configure the SSH authentication using the Kerberos protocol. The client machine will connect to the Kerberos server through SSH with the Kerberos authentication.

#### 5.1 Server setup

Adds a new system user named *sshclient*:

```
$ sudo useradd -m -s /bin/bash sshclient
```

Adds the same user from the **kadmin** level:

```
$ sudo kadmin.local
addprinc sshclient
quit
```

I need to enable the *GSSAPIAuthentication* in the ssh configuration:

```
...
GSSAPIAuthentication yes
GSSAPICleanupCredentials yes
...
```

Restarting the ssh service:

```
$ sudo systemctl restart sshd.service
$ sudo systemctl status sshd.service
```

#### 5.2 Client setup

Adds new system user called *sshclient* and logs in to the system:

```
$ sudo useradd -m -s /bin/bash sshclient
$ su - sshclient
```

Initializing the Kerberos user principal *sshclient*:

```
kinit sshclient
```

I check the available ticket:

```
sshclient@ctx06vm:~$ klist
Ticket cache: FILE:/tmp/krb5cc_1001_rw7I86
Default principal: sshclient@LOCAL.LNXORG

Valid starting       Expires              Service principal
18.03.2019 23:43:27  19.03.2019 09:43:27  krbtgt/LOCAL.LNXORG@LOCAL.LNXORG
        renew until 19.03.2019 23:43:27
```

Now I can connect to the server using the Kerberos SSH authentication.

```
$ ssh ctx12vm.local.lnxorg
```

Reference: [Kerberos](https://web.mit.edu/kerberos/).
