### OpenLiteSpeed Web Server on Debian 10

> My test platform: DebianEdu/Skolelinux 10

OpenLiteSpeed is a free, open-source and lightweight HTTP server. It provides a web-based user interface to manage web server from the web browser.

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

Update your operating system packages and install required dependencies for OpenLiteSpeed:

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install build-essential libexpat1-dev libgeoip-dev libpcre3-dev libudns-dev zlib1g-dev libssl-dev libxml2 libxml2-dev rcs \
                    libpng-dev libpng-dev openssl autoconf g++ make openssl libssl-dev libcurl4-openssl-dev libcurl4-openssl-dev \
                    pkg-config libsasl2-dev libzip-dev unzip wget
```

#### 2. OpenLiteSpeed - installation and configuration

```
$ cd /tmp
$ wget https://openlitespeed.org/packages/openlitespeed-1.5.2.tgz
$ tar -xvzf openlitespeed-1.5.2.tgz
$ cd openlitespeed
$ sudo sh install.sh
```

After the installation is complete, we configure the administrative password for the web interface:

```
$ sudo /usr/local/lsws/admin/misc/admpass.sh
```

Next, start the OpenLiteSpeed service:

```
$ sudo /etc/init.d/lsws start
```

OpenLiteSpeed is now running and listening on port 8088. Open your web browser and type the URL *http://your-server-ip:8088*.

Edit */etc/apt/sources.list.d/lst_debian_repo.list* and change the installation source to the following:

```bash
deb http://rpms.litespeedtech.com/debian buster main
```

#### 3. MariaDB - installation and configuration

```
$ sudo apt install mariadb-server mariadb-client -y
```

The service should be started automatically after packages pre-configuration is complete. You can check it and enable the start of service together with the system.

```
$ systemctl status mariadb.service
$ sudo systemctl enable mariadb.service
```

To secure MariaDB installation, use the command below:

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
$ sudo mysql
MariaDB [(none)]> USE mysql;
MariaDB [mysql]> Update user Set plugin='' WHERE User='root';
MariaDB [mysql]> FLUSH PRIVILEGES;
MariaDB [mysql]> exit
Bye
```

#### 4. PHP - installation and configuration

By default, PHP is not available in the OpenLiteSpeed repository. We compile PHP from the administration panel. To access the panel, open your web browser and enter the URL *http://your-server-ip:7080*. Provide your admin user name and password which you have created earlier and click on the **Login** button. Now:

- click in the *Tools > Compile PHP*,
- select the PHP version you want to install and click on the **Next** button,
- choose the build options and click on the **Next** button to start downloading PHP,
- once the process completed successfully click on the **Next** button.

Log in to your server from terminal and run the pre-generated script:

```
$ sudo /usr/local/lsws/phpbuild/buildphp_manual_run.sh
```

This script will download and compile PHP and related modules to your system.

Once the installation has been completed successfully, you should see the following output:

```
==============================================
Finished building PHP 7.3.6 with LSAPI
==============================================
nie, 21 lip 2019, 14:41:46 CEST

**DONE**
```

Reference: [OpenLiteSpeed](https://openlitespeed.org/#install)
