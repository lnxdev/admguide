### Short instruction how to install ssl certificate from Let’s Encrypt on Apache Tomcat

> My test platform: Debian 9.6 with Apache Tomcat 8.5.14

#### 1. Requirements

* Ports 80 (http) and 443 (https) open on the firewall.
* Valid DNS settings. Make sure that A record in DNS will resolve your address. In my example, yourhost.com. [Let's Encrypt](https://letsencrypt.org/) authenticates your host with [Domain Validation](https://letsencrypt.org/how-it-works/). You will not receive an SSL certificate from Let's Encrypt if the yourhost.com address is not resolved on the host.
* Apache Tomcat 8.5  or greater.

#### 2. Install certbot

```
$ sudo apt update
$ sudo apt install certbot
```

#### 3. Stop Tomcat

```
$ sudo systemctl stop tomcat8.service
```

#### 4. Create the SSL certificate for your domain (in my example yourhost.com)

Continue working as root.

```
# certbot certonly --standalone -d yourhost.com
```

#### 5. Verify certificates

```
# ls /etc/letsencrypt/live/yourhost.com/
cert.pem  chain.pem  fullchain.pem  privkey.pem  README
```

Next copy the files cert.pem, chain.pem and privkey.pem to *CATALINA_BASE/conf* (in my example /etc/tomcat8). Any other directory works, too. Even symlinks work, provided the permissions are set properly. Make sure not to move the *.pem files! Only copy them.

```
# cd /etc/letsencrypt/live/yourhost.com
# cp cert.pem /etc/tomcat8
# cp chain.pem /etc/tomcat8
# cp privkey.pem /etc/tomcat8
```

#### 6. Set the permissions (in my example tomcat:tomcat)

```
# chown tomcat:tomcat *.pem
```

#### 7. Edit server.xml and configure the https connector
Remove the open and closing comments and configure the connector with the *.pem files we copied earlier. This part of your *server.xml* should now look like this:

```xml
<Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" maxThreads="150" SSLEnabled="true">
  <SSLHostConfig>
    <Certificate certificateFile="conf/cert.pem"
                 certificateKeyFile="conf/privkey.pem"
                 certificateChainFile="conf/chain.pem" />
  </SSLHostConfig>
</Connector>
```

#### 8. Start Tomcat

```
# systemctl start tomcat8.service
# systemctl status tomcat8.service
```

#### 9. Refresh your certificate every 90 days

The SSL certificates provided by Let's Encrypt expire after 90 days unless you refresh them. Refreshing is easy. Just repeat steps 3 through 6 and run tomcat again. You can also script it using bash and crontab.
