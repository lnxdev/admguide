### Build the latest mainline version Nginx from source on Fedora 29

> My test platform: Fedora Server 29 and Nginx 1.15.8

#### 1. Requirements for building Nginx from source

The default build configuration Nginx depends on only 3 libraries to be installed: *OpenSSL/LibreSSL/BoringSSL*, *Zlib* and *PCRE*.

| Mandatory requirements        | Optional requirement | 
|-------------------------------|----------------------|
| GNU Compiler Collection (GCC) | Perl                 | 
| OpenSSL library version between 1.0.2 - 1.1.1 or LibreSSL library or BoringSSL library | LibGD |
| Zlib library version between 1.1.3 - 1.2.11 | MaxMind GeoIP Legacy C Library |
| PCRE library version between 4.4 - 8.42 | libxml2 |
| | libxslt |

User with root privileges or non-root user with sudo privileges.

#### 2. Build Nginx from source

Install the development tools package:

```
$ sudo dnf upgrade -y
$ sudo dnf groupinstall -y 'Development Tools'
```

[Download](http://nginx.org/en/download.html) the latest mainline version of Nginx source code and extract it. At the time of writing this tutorial, the latest version is 1.15.8.

```
$ wget https://nginx.org/download/nginx-1.15.8.tar.gz && tar zxvf nginx-1.15.8.tar.gz
```

Download the mandatory dependencies and extract them:

```
$ wget https://ftp.pcre.org/pub/pcre/pcre-8.42.tar.gz && tar zxvf pcre-8.42.tar.gz
$ wget https://www.zlib.net/zlib-1.2.11.tar.gz && tar zxvf zlib-1.2.11.tar.gz
$ wget https://www.openssl.org/source/openssl-1.1.1a.tar.gz && tar zxvf openssl-1.1.1a.tar.gz
```

Install optional dependencies:

```
$ sudo dnf install -y perl perl-devel perl-ExtUtils-Embed libxslt libxslt-devel libxml2 libxml2-devel gd gd-devel GeoIP GeoIP-devel
```

Copy Nginx manual page to */usr/share/man/man8/* directory:

```
$ sudo cp ~/nginx-1.15.8/man/nginx.8 /usr/share/man/man8
$ sudo gzip /usr/share/man/man8/nginx.8
```

Configure, compile and install:

```
$ cd ~/nginx-1.15.8
$ ./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib64/nginx/modules \
--conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --pid-path=/var/run/nginx.pid \
--lock-path=/var/run/nginx.lock --user=nginx --group=nginx --build=Fedora --builddir=nginx-1.15.8 \
--with-select_module --with-poll_module --with-threads --with-file-aio --with-http_ssl_module \
--with-http_v2_module --with-http_realip_module --with-http_addition_module --with-http_xslt_module=dynamic \
--with-http_image_filter_module=dynamic --with-http_geoip_module=dynamic --with-http_sub_module \
--with-http_dav_module --with-http_flv_module --with-http_mp4_module --with-http_gunzip_module \
--with-http_gzip_static_module --with-http_auth_request_module --with-http_random_index_module \
--with-http_secure_link_module --with-http_degradation_module --with-http_slice_module \
--with-http_stub_status_module --with-http_perl_module=dynamic --with-perl_modules_path=/usr/lib64/perl5 \
--with-perl=/usr/bin/perl --http-log-path=/var/log/nginx/access.log \ 
--http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
--http-scgi-temp-path=/var/cache/nginx/scgi_temp --with-mail=dynamic --with-mail_ssl_module \
--with-stream=dynamic --with-stream_ssl_module --with-stream_realip_module --with-stream_geoip_module=dynamic \
--with-stream_ssl_preread_module --with-compat --with-pcre=../pcre-8.42 --with-pcre-jit \ 
--with-zlib=../zlib-1.2.11 --with-openssl=../openssl-1.1.1a --with-openssl-opt=no-nextprotoneg \
--with-debug

$ make
$ sudo make install
```

Symlink */usr/lib64/nginx/modules* to */etc/nginx/modules* directory (*/etc/nginx/modules* is a standard place for Nginx modules):

```
$ sudo ln -s /usr/lib64/nginx/modules /etc/nginx/modules
```

Print the Nginx version, the compiler version and configuration parameters:

```
$ sudo nginx -V
nginx version: nginx/1.15.8 (Fedora)
built by gcc 8.2.1 20181215 (Red Hat 8.2.1-6) (GCC)
built with OpenSSL 1.1.1a  20 Nov 2018
TLS SNI support enabled
configure arguments: --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib64/nginx/modules \
--conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --pid-path=/var/run/nginx.pid \
--lock-path=/var/run/nginx.lock --user=nginx --group=nginx --build=Fedora --builddir=nginx-1.15.8 \
--with-select_module --with-poll_module --with-threads --with-file-aio --with-http_ssl_module \
--with-http_v2_module --with-http_realip_module --with-http_addition_module --with-http_xslt_module=dynamic \
--with-http_image_filter_module=dynamic --with-http_geoip_module=dynamic --with-http_sub_module \
--with-http_dav_module --with-http_flv_module --with-http_mp4_module --with-http_gunzip_module \
--with-http_gzip_static_module --with-http_auth_request_module --with-http_random_index_module \
--with-http_secure_link_module --with-http_degradation_module --with-http_slice_module \
--with-http_stub_status_module --with-http_perl_module=dynamic --with-perl_modules_path=/usr/lib64/perl5 \
--with-perl=/usr/bin/perl --http-log-path=/var/log/nginx/access.log \
--http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
--http-scgi-temp-path=/var/cache/nginx/scgi_temp --with-mail=dynamic --with-mail_ssl_module \
--with-stream=dynamic --with-stream_ssl_module --with-stream_realip_module --with-stream_geoip_module=dynamic \
--with-stream_ssl_preread_module --with-compat --with-pcre=../pcre-8.42 --with-pcre-jit \
--with-zlib=../zlib-1.2.11 --with-openssl=../openssl-1.1.1a --with-openssl-opt=no-nextprotoneg --with-debug
```

Create system group and user for Nginx:

```
$ sudo useradd --system --home /var/cache/nginx --shell /sbin/nologin --comment "nginx user" --user-group nginx
```

Create cache directories and set proper permissions:

```
$ sudo mkdir -p /var/cache/nginx/client_temp /var/cache/nginx/fastcgi_temp /var/cache/nginx/proxy_temp /var/cache/nginx/scgi_temp /var/cache/nginx/uwsgi_temp
$ sudo chmod 700 /var/cache/nginx/*
$ sudo chown nginx:root /var/cache/nginx/*
```

Check Nginx syntax:

```
$ sudo nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Create Nginx service file */etc/systemd/system/nginx.service* and paste the code below:

```
[Unit]
Description=nginx - high performance web server
Documentation=https://nginx.org/en/docs/
After=network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -c /etc/nginx/nginx.conf
ExecStart=/usr/sbin/nginx -c /etc/nginx/nginx.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID

[Install]
WantedBy=multi-user.target
```

**Make sure you've set the PID file and the Nginx binary for your configuration.**

Start the Nginx service and verify status:

```
$ sudo systemctl start nginx.service
$ sudo systemctl status nginx.service
```

If you want Nginx to start on system boot, run:

```
$ sudo systemctl enable nginx.service
```

Nginx by default, generates backup files in */etc/nginx*. Remove **.default* files from */etc/nginx* directory:

```
$ sudo rm /etc/nginx/*.default
```

Create *conf.d*, *snippets*, *sites-available* and *sites-enabled* directories in */etc/nginx* directory:

```
$ sudo mkdir /etc/nginx/{conf.d,snippets,sites-available,sites-enabled}
```

Change permissions and group ownership of Nginx log files:

```
$ sudo chmod 640 /var/log/nginx/*
$ sudo chown nginx:adm /var/log/nginx/access.log /var/log/nginx/error.log
```

Create log rotation config for Nginx. Create */etc/logrotate.d/nginx* file and paste the code below:

```
/var/log/nginx/*.log {
    daily
    missingok
    rotate 52
    compress
    delaycompress
    notifempty
    create 640 nginx adm
    sharedscripts
    postrotate
        if [ -f /var/run/nginx.pid ]; then
            kill -USR1 `cat /var/run/nginx.pid`
        fi
    endscript
}
```

**Make sure you've set the PID file for your configuration.**

Remove all downloaded files:

```
$ cd ~
$ rm -rf *.tar.gz
$ rm -rf nginx-1.15.8/ openssl-1.1.1a/ pcre-8.42/ zlib-1.2.11/
```

Now, you have the latest version of Nginx installed by building it from source code.
