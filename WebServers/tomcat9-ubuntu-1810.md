### Apache Tomcat 9 on Ubuntu 18.10

> My test platform: Ubuntu Server 18.10 and Tomcat 9.0.14

#### 1. Requirements

- A non-root user with sudo privileges.

#### 2. Java installation

```
$ sudo apt update -y
$ sudo apt upgrade -y
$ sudo apt install default-jdk
```

#### 3. Creating a Tomcat User

Tomcat should be ran with a non-privileged user i.e non root: 

```
$ sudo groupadd tomcat
$ sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
```

#### 4. Tomcat installation

To download the latest package, head to [Tomcat’s download page](https://tomcat.apache.org/download-90.cgi). At the time of writing this tutorial, the latest version of Tomcat is 9.0.14. 

```
$ cd /tmp
$ wget http://ftp.man.poznan.pl/apache/tomcat/tomcat-9/v9.0.14/bin/apache-tomcat-9.0.14.tar.gz
$ sudo mkdir /opt/tomcat
$ sudo tar xzvf apache-tomcat-9.0.14.tar.gz -C /opt/tomcat --strip-components=1
$ cd /opt/tomcat
$ sudo chgrp -R tomcat /opt/tomcat
$ sudo chmod -R g+r conf
$ sudo chmod g+x conf
$ sudo chown -R tomcat webapps/ work/ temp/ logs/
```

#### 5. Creating a SystemD Service File for Tomcat

Because I want to run Tomcat as a service, I will need to have a file which will help me manage the process. 

Tomcat have to know where java is located on system. To find java location (*JAVA_HOME*), use the following command:

```
$ sudo update-java-alternatives -l
```

Next, create Tomcat service file */etc/systemd/system/tomcat.service* and paste the code below in the file:

```
[Unit]
Description=Apache Tomcat
After=network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64
Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
```

Make sure you've set the location *JAVA_HOME* for your system. Reload the systemd daemon:

```
$ sudo systemctl daemon-reload
```

Start the Tomcat service and verify status:

```
$ sudo systemctl start tomcat.service
$ sudo systemctl status tomcat.service
```

You may need permission to access port 8080 in the firewall:

```
$ sudo ufw allow 8080
```

You can now test Tomcat by using your system’s IP address followed by the service default port 8080 (*http://ip-address:8080*).

If you want Tomcat to start on system boot, run:

```
$ sudo systemctl enable tomcat.service
```

#### 6. Tomcat configuration

I am going to add a user in file */opt/tomcat/conf/tomcat-users.xml* that will be able to access the manager and admin interfaces. To configure such user add the following line:

```
...
<tomcat-users xmlns="http://tomcat.apache.org/xml"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd"
              version="1.0">
              
<user username="sysadmin" password="sysadminpass" roles="manager-gui,admin-gui"/>

</tomcat-users>
```

- **username** – the user you want to authenticate
- **password** – the password you want to use for authentication.

By default, access to the **Host Manager** and **Manager** is limited. To delete or change these restrictions, you can edit the following files:
- for **Manager** app - */opt/tomcat/webapps/manager/META-INF/context.xml*
- for **Host manager** app - */opt/tomcat/webapps/host-manager/META-INF/context.xml*

Inside these files you can either comment the IP restriction or allow access from your IP address. Example for **Manager** app and commented line:

```xml
...
<Context antiResourceLocking="false" privileged="true" >
<!--  <Valve className="org.apache.catalina.valves.RemoteAddrValve"
         allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" /> -->
  <Manager sessionAttributeValueClassNameFilter="java\.lang\.(?:Boolean|Integer|Long|Number|String)|org\.apache\.catalina\.filters\.CsrfPreventionFilter\$LruCache(?:\$1)?|java\.util\.(?:Linked)?HashMap"/>
</Context>
```

Reload the tomcat service and verify status:

```
$ sudo systemctl restart tomcat.service
$ sudo systemctl status tomcat.service 
```

You can now test the **Manager** app by accessing *http://ip-address:8080/manager/* and **Host Manager** app by accessing *http://ip-address:8080/host-manager/*.

#### 7. Testing Apache Tomcat by creating a test file

You can check if everything is working correctly, for example by creating a test file inside of */opt/tomcat/webapps/ROOT/* directory. Inside that file (in my case *test.jsp*), paste the following code:

```jsp
<html>
  <head>
    <title>Tomcat Test</title>
  </head>
  <body>
    <% out.println("Tomcat Test! Running JSP Application");  %>
  </body>
</html>
```

Save the file and set the ownership:

```
$ sudo chown tomcat: /opt/tomcat/webapps/ROOT/test.jsp
```

Open this file in your browser using *http://ip-address:8080/test.jsp*. You should see "**Tomcat Test! Running JSP Application**".

Reference: [Apache Tomcat 9 Documentation](http://tomcat.apache.org/tomcat-9.0-doc/).
