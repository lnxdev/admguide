### Nginx with virtual hosts and self-signed ssl certificate

> My test platform: Centos Linux 7.5.1804

#### 1. Install Nginx

``` 
------------ On Ubuntu ------------
$ sudo apt update 
$ sudo apt install nginx 

------------ On CentOS ------------
$ sudo yum update 
$ sudo yum install epel-release 
$ sudo yum install nginx

$ sudo systemctl start nginx.service
$ sudo systemctl enable nginx.service
```

#### 2. Verify the installation

```
$ sudo systemctl status nginx.service
$ sudo netstat -tlpn | grep nginx
```

#### 3. Firewall configuration

```
------------ On Ubuntu ------------
$ sudo ufw allow 80/tcp
$ sudo ufw allow 443/tcp
$ sudo ufw reload

------------ On CentOS ------------
$ sudo firewall-cmd --permanent --add-port=80/tcp
$ sudo firewall-cmd --permanent --add-port=443/tcp
$ sudo firewall-cmd --reload
```

The following is a sample Nginx configuration file (*/etc/nginx/nginx.conf*), where the http block contains an *include* directive which tells Nginx where to find website configuration files (virtual host configurations).

```
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    include /etc/nginx/conf.d/*.conf;
    ...
```

Sample server block configuration.

```
...
server {
    listen       80 default_server;
    listen       [::]:80 default_server;
    server_name  _;
    root         /usr/share/nginx/html;

    # Load configuration files for the default server block.
    include /etc/nginx/default.d/*.conf;

    location / {
    }

    error_page 404 /404.html;
        location = /40x.html {
    }

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
    }
}
...
```

#### 4. Restricted access to a web page

```
$ yum install httpd-tools               #RHEL/CentOS
$ sudo apt install apache2-utils        #Debian/Ubuntu
$ sudo htpasswd -c /etc/nginx/conf.d/.htpasswd admin
$ sudo chmod 640 /etc/nginx/conf.d/.htpasswd
$ sudo chown nginx:nginx /etc/nginx/conf.d/.htpasswd
```

Example how to password protect the directory */usr/share/nginx/html/protected/*.

```
...
    location /protected/ {
        auth_basic              "Restricted Access!";
        auth_basic_user_file    /etc/nginx/conf.d/.htpasswd;
    }
...
```

Save changes and restart Nginx service.

```
$ sudo systemctl restart nginx.service
```

#### 5. Name-based Virtual hosts in Nginx

Examples of virtual hosts:

-   firsthost.com – */var/www/html/firsthost.com/*
-   secondhost.com – */var/www/html/secondhost.com/*

Make directories and the appropriate permissions on the directories for each site.

```
$ sudo mkdir -p /var/www/html/firsthost.com/public_html 
$ sudo mkdir -p /var/www/html/secondhost.com/public_html 
$ sudo chmod -R 755 /var/www/html/firsthost.com/public_html  
$ sudo chmod -R 755 /var/www/html/secondhost.com/public_html
```

Server block configuration files for each site inside the */etc/nginx/conf.d/* directory.

-   firsthost.com – */etc/nginx/conf.d/firsthost.com.conf*

```
server {
    listen         80;
    server_name  firsthost.com;
    root           /var/www/html/firsthost.com/public_html ;
    index          index.html;
    location / {
                try_files $uri $uri/ =404;
        }
     
}
```

-   secondhost.com – */etc/nginx/conf.d/secondhost.com.conf*

```
server {
    listen         80;
    server_name    secondhost.com;
    root           /var/www/html/secondhost.com/public_html;
    index          index.html;
    location / {
                try_files $uri $uri/ =404;
        }
     
}
```

Save changes and restart Nginx service.

```
$ sudo systemctl restart nginx.service
```

#### 6. Self-signed ssl certificate

Create a directory where your certificates will be stored.

```
$ sudo mkdir /etc/nginx/ssl-certs/
```

Generate self-signed certificate:

```
$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl-certs/nginx.key -out /etc/nginx/ssl-certs/nginx.crt
```

Add the ssl directive to virtual host configuration file. Example for **firsthost.com** virtual host below.

```
server {
    listen 80;
    listen [::]:80;
    listen 443 ssl;
    listen [::]:443 ssl;
    
    ssl_certificate /etc/nginx/ssl-certs/nginx.crt;
    ssl_trusted_certificate /etc/nginx/ssl-certs/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl-certs/nginx.key;
    
    server_name    firsthost.com;
    root           /var/www/html/firsthost.com/public_html;
    index          index.html;
    location / {
                try_files $uri $uri/ =404;
        }
}
```

Restart nginx:

```
$ sudo systemctl restart nginx.service
```

If you would like to purchase an SSL certificate from a CA, you need to generate a certificate signing request (CSR) as shown.

```
$ sudo openssl req -newkey rsa:2048 -nodes -keyout /etc/nginx/ssl-certs/example.com.key -out /etc/nginx/ssl-certs/example.com.csr
```

You can also create a CSR from an existing private key.

```
$ sudo openssl req -key /etc/nginx/ssl-certs/example.com.key -new -out /etc/nginx/ssl-certs/example.com.csr
```

Then, you need to send the *CSR* to a *CA* to request the issuance of a CA-signed SSL certificate. Once you receive your certificate from the CA, you can configure it as shown above.

---

### Update 1 - HTTP/2 in Nginx

**Requirements**

* Nginx version 1.9.5 or greater. 
* OpenSSL version 1.0.2 or greater. 
* SSL/TLS certificate (Let's Encrypt or a self-signed certificate).
* TLS 1.2 or higher protocol enabled.

To enable HTTP/2 add the http2 parameter to the *listen* directive and TLS protocol to the *ssl_protocols* in virtual host:

```
...
listen 443 ssl http2;
...
ssl_protocols TLSv1.2;
...
```

And restart nginx:

```
$ sudo systemctl restart nginx.service
$ sudo systemctl status nginx.service
```

Example the minimal virtual server configuration with enabled HTTP/2:

```
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    
    ssl_certificate /etc/nginx/ssl-certs/nginx.crt;
    ssl_trusted_certificate /etc/nginx/ssl-certs/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl-certs/nginx.key;
    
    ssl_protocols TLSv1.2;
    
    server_name    thirdhost.com;
    root           /var/www/html/thirdhost.com/public_html;
    index          index.html;
    location / {
                try_files $uri $uri/ =404;
        }
}
```

To check if your server supports HTTP/2, you can use your browser dev tools or Nginx log files. 
