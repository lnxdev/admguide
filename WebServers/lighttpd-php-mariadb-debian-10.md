### Lighttpd with PHP and MariaDB on Debian 10

> My test platform: Debian 10, grml64 2018.12

#### 1. Requirements

- User with root privileges.

Update your operating system packages and install some essential packages for basic administration of the operating system:

```
# apt update && apt upgrade -y
# apt install -y wget vim git unzip mc curl net-tools
```

#### 2. Lighttpd and MariaDB installation

```
# apt install -y lighttpd mariadb-server mariadb-client
```

On Debian 10, services should be started and turned on automatically after packages pre-configuration is complete, so there is no need to start and enable them manually. You can check this using the *systemctl* command:

```
# systemctl status lighttpd.service
# systemctl status mariadb.service
```

To secure MariaDB installation, use the command below:

```
# mysql_secure_installation
```

Answer all the questions as shown below:

```
    Enter current password for root (enter for none):
    Set root password? [Y/n]: Y
    Remove anonymous users? [Y/n]: Y
    Disallow root login remotely? [Y/n]: Y
    Remove test database and access to it? [Y/n]:  Y
    Reload privilege tables now? [Y/n]:  Y
```

Log in to MariaDB shell and set up:

```
# mysql -u root -p
Enter password:

MariaDB [(none)]> USE mysql;
MariaDB [mysql]> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
MariaDB [mysql]> FLUSH PRIVILEGES;
MariaDB [mysql]> exit
```

Restart MariaDB service:

```
# systemctl restart mariadb.service
# systemctl status mariadb.service
```

#### 3. PHP installation

```
# apt install -y php php-cgi php-fpm php-mysql
```

Once the installation has been completed, edit /etc/php/7.3/fpm/php.ini and set:

```
[...]
cgi.fix_pathinfo=1
[...]
```

Edit the file */etc/php/7.3/fpm/pool.d/www.conf*. Find the following line:

```
listen = /run/php/php7.3-fpm.sock
```

and replace it with the following line:

```
listen = 127.0.0.1:9000
```

Restart the php-fpm service:

```
# systemctl restart php7.3-fpm.service
# systemctl status php7.3-fpm.service
```

Edit the file */etc/lighttpd/conf-available/15-fastcgi-php.conf*. Find the following line:

```
"bin-path" => "/usr/bin/php-cgi",
"socket" => "/var/run/lighttpd/php.socket",
```

and replace them with the following:

```
"host" => "127.0.0.1",
"port" => "9000",
```

Enable the *FastCGI*, *FastCGI-PHP* module and restart Lighttpd:

```
# lighty-enable-mod fastcgi
# lighty-enable-mod fastcgi-php
# systemctl restart lighttpd.service
# systemctl status lighttpd.service
```

#### 4. Lighttpd virtual host configuration

Create an Lighttpd virtual host file */etc/lighttpd/conf-available/vhost.conf*:

```
$HTTP["host"] == "ctx17vm.local.lnx" {
    server.document-root = "/var/www/html/"
    server.errorlog      = "/var/log/lighttpd/ctx17vm.local.lnx-error.log"
}
```

Enable the virtual host:

```
# ln -s /etc/lighttpd/conf-available/vhost.conf /etc/lighttpd/conf-enabled/
```

Change the ownership of the Lighttpd document root directory to www-data and restart Lighttpd:

```
# chown -R www-data: /var/www/html/
# systemctl restart lighttpd.service 
# systemctl status lighttpd.service 
```

---

### Update 1 - Lighttpd and Let's Encrypt Free SSL

```
# apt install -y certboot
```

Create a Let’s Encrypt certificate:

```
# certbot certonly --webroot -w /var/www/html/ -d ctx17vm.local.lnx

Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator webroot, Installer None
Enter email address (used for urgent renewal and security notices) (Enter 'c' to
cancel): admin@ctx17vm.local.lnx

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
agree in order to register with the ACME server at
https://acme-v02.api.letsencrypt.org/directory
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(A)gree/(C)ancel: A

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about our work
encrypting the web, EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: N

[...]

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/ctx17vm.local.lnx/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/ctx17vm.local.lnx/privkey.pem
   Your cert will expire on 2019-12-26. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

# cat /etc/letsencrypt/live/ctx17vm.local.lnx/cert.pem /etc/letsencrypt/live/ctx17vm.local.lnx/privkey.pem > /etc/letsencrypt/live/ctx17vm.local.lnx/web.pem  
```

Edit the Lighttpd virtual host file and define the Let's Encrypt SSL certificate path:

```
$HTTP["host"] == "ctx17vm.local.lnx" {
    server.document-root = "/var/www/html/"
}

$SERVER["socket"] == ":443" {
ssl.engine = "enable"
ssl.pemfile = "/etc/letsencrypt/live/ctx17vm.local.lnx/web.pem"
ssl.ca-file = "/etc/letsencrypt/live/ctx17vm.local.lnx/chain.pem"
server.name = "ctx17vm.local.lnx"
server.document-root = "/var/www/html/"
server.errorlog = "/var/log/lighttpd/ctx17vm.local.lnx-error.log"
accesslog.filename = "/var/log/lighttpd/ctx17vm.local.lnx-access.log"
}

$HTTP["scheme"] == "http" {
$HTTP["host"] == "ctx17vm.local.lnx" {
url.redirect = ("/.*" => "https://ctx17vm.local.lnx$0")}
}
```

Restart Lighttpd:

```
# systemctl restart lighttpd.service
# systemctl status lighttpd.service
```

Reference: [Lighttpd Docs](https://redmine.lighttpd.net/projects/lighttpd/wiki/Docs)
