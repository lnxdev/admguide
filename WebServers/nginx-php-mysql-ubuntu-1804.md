### Nginx with PHP and MySQL on Ubuntu 18.04 LTS

> My test platform: Ubuntu Server 18.04.2

#### 1. Requirements

- User with root privileges or non-root user with sudo privileges.

#### 2. Nginx installation

```
$ sudo apt update && sudo apt upgrade -y
$ sudo apt install nginx -y
```

The services should be started automatically after packages pre-configuration is complete. You can check it and enable the start of services together with the system.

```
$ systemctl status nginx.service
$ sudo systemctl enable nginx.service
```

#### 3. Firewall configuration

```
$ sudo ufw allow ssh
$ sudo ufw allow http
$ sudo ufw allow https
$ sudo ufw enable
$ sudo ufw status
```

#### 4. MySQL - installation and configuration

```
$ sudo apt install mysql-server mysql-client -y
$ systemctl status mysql.service
$ sudo systemctl enable mysql.service
```

To secure MySLQ installation, use the command below:

```
$ sudo mysql_secure_installation
```

Answer all the questions as shown below:

```
    VALIDATE PASSWORD PLUGIN can be used to test passwords
    and improve security. It checks the strength of password
    and allows the users to set only those passwords which are
    secure enough. Would you like to setup VALIDATE PASSWORD plugin?

    Press y|Y for Yes, any other key for No: Y

    Enter current password for root (enter for none):
    Set root password? : Y
    Remove anonymous users? : Y
    Disallow root login remotely? : Y
    Remove test database and access to it? :  Y
    Reload privilege tables now? :  Y
```

#### 5. PHP-FPM installation

```
$ sudo apt install php7.2 php7.2-fpm php7.2-cli php7.2-curl php7.2-mysql php7.2-curl php7.2-gd php7.2-mbstring php-pear -y
$ systemctl status php7.2-fpm.service
$ sudo systemctl enable php7.2-fpm.service
```

PHP7.2-FPM running on Ubuntu 18.04 under the sock file. Check php-fpm installation using the netstat command.

```
$ sudo apt install net-tools -y && sudo netstat -pl | grep php
```

#### 6. Nginx and PHP-FPM configuration

#### 6.1 Nginx configuration

Edit */etc/nginx/nginx.conf* and set:

```
...
keepalive_timeout 65;
...
server_tokens off;
...
```

Now edit the default Nginx virtual host file */etc/nginx/sites-available/default*. Uncomment the PHP line shown below and change the sock file line.

```
        ...
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
        #
        # # With php-fpm (or other unix sockets):
                fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        # # With php-cgi (or other tcp sockets):
        # fastcgi_pass 127.2.0.1:9000;
        }
        ...
```

Test Nginx configuration and make sure there is no error, then restart the service.

```
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ systemctl status nginx.service
```

#### 6.2 Configure PHP-FPM

Uncomment the **cgi.fix_pathinfo** line and change the value to **0** in */etc/php/7.2/fpm/php.ini*.

```
...
cgi.fix_pathinfo=0
...
```

Restart the PHP-FPM service and check status:

```
$ sudo systemctl restart php7.2-fpm.service
$ systemctl status php7.2-fpm.service
```

---

### Update 1 - PhpMyAdmin - installation and configuration

```
$ sudo apt install phpmyadmin -y
```

For the phpmyadmin database configuration, choose **Yes**.

During the installation you will be asked about the configuration of the web server for phpmyadmin. Only choose **OK**.

Go to the */etc/nginx* configuration directory, and edit the default virtual host file */etc/nginx/sites-available/default*. Paste the following Nginx configuration for phpmyadmin inside the server *{...}* bracket.

```
        ...
        location /phpmyadmin {
               root /usr/share/;
               index index.php index.html index.htm;
               location ~ ^/phpmyadmin/(.+\.php)$ {
                       try_files $uri =404;
                       root /usr/share/;
                       fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
                       fastcgi_index index.php;
                       fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                       include /etc/nginx/fastcgi_params;

               }
               location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
                       root /usr/share/;
               }
        }
        location /phpMyAdmin {
               rewrite ^/* /phpmyadmin last;
        }
        ...
```

Test Nginx configuration and make sure there is no error, then restart the service.

```
$ sudo nginx -t
$ sudo systemctl restart nginx.service
$ systemctl status nginx.service
```
